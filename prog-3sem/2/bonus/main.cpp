#include <iostream>
#include <string>
#include <vector>
#include <stack>

using std::string;
using std::vector;
using std::min;
using std::max;

enum { alphabet_size = 256, first_ascii_index = 48 };

void GetLCP(string& str, vector<int>& permutations_index,
            vector<vector<int>>& classes, int power_of_two, vector<int>& lcp) {
  for (int i = 1; i < str.length() - 1; ++i) {
    int x = permutations_index[i];
    int y = permutations_index[i + 1];
    for (int j = power_of_two - 1; j >= 0; --j)
      if (classes[j][x] == classes[j][y]) {
        lcp[i - 1] += 1 << j;
        x += 1 << j;
        y += 1 << j;
      }
  }
}

void GetStartClassIndex(string& str, vector<int>& start_class_index) {
  for (char c : str) {
    ++start_class_index[c - first_ascii_index + 1];
  }

  for (int i = 1; i < alphabet_size; ++i) {
    start_class_index[i] += start_class_index[i - 1];
  }
}

void GetClassesForTwoCharStr(vector<int>& permutations_index, string& str,
                             vector<vector<int>>& classes, int& classes_count) {
  classes[0][permutations_index[0]] = 0;
  for (int i = 1; i < str.length(); ++i) {
    if (str[permutations_index[i]] != str[permutations_index[i - 1]]) {
      ++classes_count;
    }
    classes[0][permutations_index[i]] = classes_count - 1;
  }
}

void GetClasses(string& str, vector<int>& permutations_index, int& two_power,
                vector<vector<int>>& classes, int& classes_count) {
  int str_len = str.length();
  vector<int> new_permutations_index(str_len);
  for (; (1 << two_power) < str_len; ++two_power) {
    for (int i = 0; i < str_len; ++i) {
      new_permutations_index[i] = permutations_index[i] - (1 << two_power);
      if (new_permutations_index[i] < 0) {
        new_permutations_index[i] += str_len;
      }
    }

    vector<int> start_class_index(classes_count, 0);
    for (int i = 0; i < str_len; ++i) {
      ++start_class_index[classes[two_power][new_permutations_index[i]]];
    }
    for (int i = 1; i < classes_count; ++i) {
      start_class_index[i] += start_class_index[i - 1];
    }
    for (int i = str_len - 1; i >= 0; --i) {
      --start_class_index[classes[two_power][new_permutations_index[i]]];
      permutations_index
      [start_class_index[classes[two_power][new_permutations_index[i]]]] =
              new_permutations_index[i];
    }

    // Считаем новые классы эквивалентности
    classes.emplace_back(str_len);
    classes[two_power + 1][permutations_index[0]] = 0;
    classes_count = 1;
    for (int i = 1; i < str_len; ++i) {
      int first_mid = (permutations_index[i] + (1 << two_power)) % str_len;
      int second_mid = (permutations_index[i - 1] + (1 << two_power)) % str_len;
      if (classes[two_power][permutations_index[i]] !=
          classes[two_power][permutations_index[i - 1]] ||
          classes[two_power][first_mid] != classes[two_power][second_mid]) {
        ++classes_count;
      }
      classes[two_power + 1][permutations_index[i]] = classes_count - 1;
    }
  }
}

void GetSortPermutations(string& str, vector<vector<int>>& classes,
                         vector<int>& permutations_index, int& two_power) {
  // Cортировка подсчётом + разделение на классы эквивалентности
  vector<int> start_class_index(alphabet_size + 1, 0);
  GetStartClassIndex(str, start_class_index);
  for (int i = 0; i < str.length(); ++i) {
    --start_class_index[str[i] - first_ascii_index + 1];
    permutations_index[start_class_index[str[i] - first_ascii_index + 1]] = i;
  }

  // Считаем количество классов эквивалентности для строк из двух символов
  int classes_count = 1;
  GetClassesForTwoCharStr(permutations_index, str, classes, classes_count);

  // Повторяем алгоритм для степеней двойки, пока можем
  GetClasses(str, permutations_index, two_power, classes, classes_count);
}

void GetRefren(string& str,int& refren_length, int& start_refren_index,
               int& max_count) {
  vector<int> permutations_index(str.length());
  vector<vector<int>> classes(1, vector<int>(str.length()));
  int two_power = 0;
  vector<int> lcp(str.length() - 2, 0);

  GetSortPermutations(str, classes, permutations_index, two_power);
  GetLCP(str, permutations_index, classes, two_power, lcp);

  std::stack<vector<int>> st;
  vector<int> cur_data(3);

  for (int i = 0; i < str.length(); ++i) {
    int cur_count = 1;
    while(!st.empty() && lcp[i] <= st.top()[2]) {
      int prev_count = st.top()[0];
      int cur_index = st.top()[1];
      int cur_len = st.top()[2];

      st.pop();
      cur_count += prev_count;
      if (cur_count * cur_len > max_count * refren_length) {
        max_count = cur_count;
        refren_length = cur_len;
        start_refren_index = permutations_index[cur_index];
      }
    }
    if (st.empty() || lcp[i] > st.top()[2]) {
      cur_data[0] = cur_count;
      cur_data[1] = i;
      cur_data[2] = lcp[i];
      st.push(cur_data);
    }
  }

}

int main() {
  int n;
  int m;
  std::cin >> n >> m;
  vector<int> sequence(n);
  string str = "";
  for(int i = 0; i < n; ++i) {
    std::cin >> sequence[i];
    str.push_back(char(sequence[i]) + first_ascii_index);
  }
  int refren_length = 1;
  int start_refren_index = 0;
  int max_count = 1;
  GetRefren(str, refren_length, start_refren_index, max_count);

  std::cout << refren_length*max_count << "\n" << refren_length <<"\n";

  for(int i = start_refren_index; i < start_refren_index + max_count; ++i) {
    std::cout<<sequence[i] << " ";
  }
  return 0;
}