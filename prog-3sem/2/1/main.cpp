#include <cmath>
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

enum { alphabet_size = 256, first_ascii_index = 1 };

void GetLCP(const string& str, const vector<int>& permutations_index,
            const vector<vector<int>>& classes, vector<int>& lcp) {
  int two_power = pow(classes.size(), 1 / 2) + 1;
  for (int i = 1; i < str.length() - 1; ++i) {
    int x = permutations_index[i];
    int y = permutations_index[i + 1];
    for (int j = two_power - 1; j >= 0; --j)
      if (classes[j][x] == classes[j][y]) {
        lcp[i - 1] += 1 << j;
        x += 1 << j;
        y += 1 << j;
      }
  }
}

void GetStartClassIndex(const string& str, vector<int>& start_class_index) {
  for (char c : str) {
    ++start_class_index[c - first_ascii_index + 1];
  }

  for (int i = 1; i < alphabet_size; ++i) {
    start_class_index[i] += start_class_index[i - 1];
  }
}

void GetClassesForTwoCharStr(const vector<int>& permutations_index,
        const string& str, vector<vector<int>>& classes, int& classes_count) {
  classes[0][permutations_index[0]] = 0;
  for (int i = 1; i < str.length(); ++i) {
    if (str[permutations_index[i]] != str[permutations_index[i - 1]]) {
      ++classes_count;
    }
    classes[0][permutations_index[i]] = classes_count - 1;
  }
}

void GetClasses(const string& str, vector<int>& permutations_index,
                vector<vector<int>>& classes, int& classes_count) {
  int two_power = 0;
  int str_len = str.length();
  vector<int> new_permutations_index(str_len);
  for (; (1 << two_power) < str_len; ++two_power) {
    for (int i = 0; i < str_len; ++i) {
      new_permutations_index[i] = permutations_index[i] - (1 << two_power);
      if (new_permutations_index[i] < 0) {
        new_permutations_index[i] += str_len;
      }
    }

    vector<int> start_class_index(classes_count, 0);
    for (int i = 0; i < str_len; ++i) {
      ++start_class_index[classes[two_power][new_permutations_index[i]]];
    }
    for (int i = 1; i < classes_count; ++i) {
      start_class_index[i] += start_class_index[i - 1];
    }
    for (int i = str_len - 1; i >= 0; --i) {
      --start_class_index[classes[two_power][new_permutations_index[i]]];
      permutations_index
      [start_class_index[classes[two_power][new_permutations_index[i]]]] =
              new_permutations_index[i];
    }

    // Считаем новые классы эквивалентности
    classes.emplace_back(str_len);
    classes[two_power + 1][permutations_index[0]] = 0;
    classes_count = 1;
    for (int i = 1; i < str_len; ++i) {
      int first_mid = (permutations_index[i] + (1 << two_power)) % str_len;
      int second_mid = (permutations_index[i - 1] + (1 << two_power)) % str_len;
      if (classes[two_power][permutations_index[i]] !=
          classes[two_power][permutations_index[i - 1]] ||
          classes[two_power][first_mid] != classes[two_power][second_mid]) {
        ++classes_count;
      }
      classes[two_power + 1][permutations_index[i]] = classes_count - 1;
    }
  }
}

int GetDifferentSubstringsCount(string& str) {
  str.push_back(first_ascii_index - 1);
  vector<int> permutations_index(str.length());

  // Cортировка подсчётом + разделение на классы эквивалентности
  vector<int> start_class_index(alphabet_size + 1, 0);
  GetStartClassIndex(str, start_class_index);
  for (int i = 0; i < str.length(); ++i) {
    --start_class_index[str[i] - first_ascii_index + 1];
    permutations_index[start_class_index[str[i] - first_ascii_index + 1]] = i;
  }

  // Считаем количество классов эквивалентности для строк из двух символов
  vector<vector<int>> classes(1, vector<int>(str.length()));
  int classes_count = 1;
  GetClassesForTwoCharStr(permutations_index, str, classes, classes_count);

  // Повторяем алгоритм для степеней двойки, пока можем
  GetClasses(str, permutations_index, classes, classes_count);

  vector<int> lcp(str.length() - 2, 0);
  GetLCP(str, permutations_index, classes, lcp);

  // Считаем количество различных подстрок
  int different_substrings_count = -lcp[0];
  for (int i = 1; i < str.length(); ++i) {
    different_substrings_count += (str.length() - 1) - permutations_index[i];
    if (i < str.length() - 2) {
      different_substrings_count -= lcp[i];
    }
  }
  return different_substrings_count;
}

int main() {
  string str;
  std::cin >> str;
  std::cout << GetDifferentSubstringsCount(str);
}