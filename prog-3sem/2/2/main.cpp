#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <cassert>

class SuffixTreeNode;

// Кодирует положение при обходе сжатого суффиксного дерева:
// вершина, в которую ведет ребро и длинна оставшейся подстроки.
struct Position {
    Position();

    Position(SuffixTreeNode *finish, size_t distance) :
            finish(finish), distanceToFinish(distance) { }

    // Проверяет, является ли соответствующая вершина явной.
    bool isExplicit() const;

    // Указывает на явную вершину, в которой закачивается ребро, на котором мы находимся.
    SuffixTreeNode *finish;
    // Длинна строки оставшейся до конца ребра, начиная с текущего положения.
    size_t distanceToFinish;
};

// Представляет явную вершину в суффиксном дереве.
class SuffixTreeNode {
public:

    SuffixTreeNode(SuffixTreeNode *parent, size_t labelBegin, size_t labelEnd) :
            parent(parent), labelBegin(labelBegin), labelEnd(labelEnd) {
      suffixLink = nullptr;
    }

    ~SuffixTreeNode();

    // Добавляет переход из вершины в сторону другой вершины node по заданому символу.
    void addLink(SuffixTreeNode *node, char letter);

    void eraseLink(char letter);

    void clearLinks();

    // Индекс первого символа метки ребра, ведущего из предка.
    size_t getLabelBegin() const;

    void setLabelBegin(size_t labelBegin);

    // Индекс символа за последним символом метки ребра, ведущего из предка.
    size_t getLabelEnd() const;

    void setLabelEnd(size_t labelEnd);

    // Проверяем, существует ли ребро из данной вершины по заданной букве.
    bool canGo(char letter);

    // Если возможно, возвращает позицию в дереве,
    // соответствующую переходу из данной вершины по заданой букве.
    Position go(char letter);

    // Возвращает позицию в дереве, соответствующую данной явной вершине.
    Position getPosition();

    // Указатель на предка.
    SuffixTreeNode * getParent() const;

    void setParent(SuffixTreeNode *parent);

    SuffixTreeNode * getSuffixLink() const;

    void setSuffixLink(SuffixTreeNode *suffixLink);

    long long int getEdgeLength() const;

    long long int countSubstrings() const;

private:
    std::map<char, Position> links;
    SuffixTreeNode *parent;
    SuffixTreeNode *suffixLink;
    size_t labelBegin, labelEnd;
};

class SuffixTree {
public:

    explicit SuffixTree(const std::string &text);

    ~SuffixTree();

    SuffixTreeNode *getRoot() const { return root; }

    const std::string &getText() const { return text; }

    // Печатает дерево в выходной поток output.
    void printTree(std::ostream &output) const;

    // Проверяет возможен ли в дереве переход из позиции по заданой букве.
    bool canGo(const Position &position, char letter) const;

    // Если возможно, возвращает позицию в дереве,
    // соответствующую переходу из заданой позиции по заданой букве.
    Position go(const Position &position, char letter);

    // Делает вершину, соотвестующую позиции, явной.
    // Если она была неявная, то разрезает ребро и создает новую явную вершину.
    // Иначе просто возвращает эту явную вершину.
    SuffixTreeNode *makeExplicit(const Position &position);

    // Считает количество различных подстрок в дереве.
    long long int countSubstrings() const;

private:
    std::string text;
    SuffixTreeNode *root, *blank;
};

void SuffixTreeNode::addLink(SuffixTreeNode *node, char letter) {
  size_t distance = node->getLabelEnd() - node->getLabelBegin();
  links.insert(std::make_pair(letter, Position(node, distance - 1)));
}

void SuffixTreeNode::eraseLink(char letter) {
  links.erase(letter);
}

void SuffixTreeNode::clearLinks() {
  links.clear();
}

void SuffixTreeNode::setLabelBegin(size_t labelBegin) {
  SuffixTreeNode::labelBegin = labelBegin;
}

size_t SuffixTreeNode::getLabelBegin() const {
  return labelBegin;
}

void SuffixTreeNode::setLabelEnd(size_t labelEnd) {
  SuffixTreeNode::labelEnd = labelEnd;
}

size_t SuffixTreeNode::getLabelEnd() const {
  return labelEnd;
}

SuffixTreeNode *SuffixTreeNode::getParent() const {
  return parent;
}

void SuffixTreeNode::setParent(SuffixTreeNode *parent) {
  this->parent = parent;
}

bool SuffixTreeNode::canGo(char letter) {
  return links.find(letter) != links.end();
}

void SuffixTreeNode::setSuffixLink(SuffixTreeNode *suffixLink) {
  SuffixTreeNode::suffixLink = suffixLink;
}

SuffixTreeNode *SuffixTreeNode::getSuffixLink() const {
  return suffixLink;
}

Position SuffixTreeNode::getPosition() {
  return Position(this, 0);
}

Position SuffixTreeNode::go(char letter) {
  if (!canGo(letter)) {
    throw std::logic_error("No such link.");
  }
  else {
    return links.find(letter)->second;
  }
}

Position::Position() {
  finish = nullptr;
  distanceToFinish = 0;
}

bool Position::isExplicit() const {
  return distanceToFinish == 0;
}

long long SuffixTreeNode::countSubstrings() const {
  long long sum = getEdgeLength();
  for (auto pair : links) {
    sum += pair.second.finish->countSubstrings();
  }
  return sum;
}

long long int SuffixTreeNode::getEdgeLength() const {
  return (long long int) (getLabelEnd() - getLabelBegin());
}

SuffixTreeNode::~SuffixTreeNode() {
  for (auto pair : links) {
    // Рекурсивно удаляем потомков.
    if (pair.second.finish != nullptr) {
      delete pair.second.finish;
    }
  }
}

SuffixTree::SuffixTree(const std::string &text) : text(text) {
  // Создаем фиктивную вершину для унификации операций.
  blank = new SuffixTreeNode(nullptr, 0, 0);
  root = new SuffixTreeNode(nullptr, std::numeric_limits<size_t>::max(), 0);
  // Из фиктивной вершины в root ведут ребра со всеми символами алфавита.
  for (char c = 'a'; c <= 'z'; ++c) {
    blank->addLink(root, c);
  }
  root->setSuffixLink(blank);
}

bool SuffixTree::canGo(const Position &position, char letter) const {
  if (position.isExplicit()) {
    return position.finish->canGo(letter);
  } else {
    size_t nextLetterIndex = position.finish->getLabelEnd() - position.distanceToFinish;
    assert(nextLetterIndex < text.length());
    return text[nextLetterIndex] == letter;
  }
}

Position SuffixTree::go(const Position &position, char letter) {
  if (position.isExplicit()) {
    return position.finish->go(letter);
  } else {
    if (canGo(position, letter)) {
      return Position(position.finish, position.distanceToFinish - 1);
    } else {
      throw std::logic_error("Wrong letter.");
    }
  }
}

SuffixTreeNode *SuffixTree::makeExplicit(const Position &position) {
  SuffixTreeNode *finishNode = position.finish;

  if (position.isExplicit()) {
    return finishNode;
  }

  size_t currentLetterIndex = finishNode->getLabelEnd() - position.distanceToFinish;
  SuffixTreeNode *parent = finishNode->getParent();
  SuffixTreeNode *newNode = new SuffixTreeNode(parent,
                                               finishNode->getLabelBegin(),
                                               currentLetterIndex);

  finishNode->setLabelBegin(currentLetterIndex);
  finishNode->setParent(newNode);
  finishNode->setParent(newNode);

  parent->eraseLink(text[newNode->getLabelBegin()]);
  parent->addLink(newNode, text[newNode->getLabelBegin()]);

  newNode->addLink(finishNode, text[finishNode->getLabelBegin()]);
  return newNode;
}

long long int SuffixTree::countSubstrings() const {
  return root->countSubstrings() - 1;
}

SuffixTree::~SuffixTree() {
  delete root;
  // Из blank идут несколько ребер в одну уже удаленную вершину.
  // Очищаем список его детей, что бы не удалять их.
  blank->clearLinks();
  delete blank;
}