#include <string>
#include <vector>

enum {
  system_number = 10,
  ascii_first_digit_index = 48,
  min_length_for_karatsuba = 4
};

using std::max;
using std::string;
using std::vector;

class BigInteger {
 public:
  BigInteger();

  // конструирование из int
  BigInteger(const int &);

  // конструирование из string
  BigInteger(const string &);

  // неявное преобразование в bool
  explicit operator bool();

  // деструктор
  ~BigInteger();

  // Сложение
  friend BigInteger operator+(const BigInteger &, const BigInteger &);

  // Вычитание
  friend BigInteger operator-(const BigInteger &, const BigInteger &);

  // Умножение
  friend BigInteger operator*(const BigInteger &, const BigInteger &);

  // Деление
  friend BigInteger operator/(const BigInteger &, const BigInteger &);

  // Остаток по модулю
  friend BigInteger operator%(const BigInteger &, const BigInteger &);

  // +=
  BigInteger &operator+=(const BigInteger &value);

  // -=
  BigInteger &operator-=(const BigInteger &value);

  // *=
  BigInteger &operator*=(const BigInteger &value);

  // /=
  BigInteger &operator/=(const BigInteger &value);

  // %=
  BigInteger &operator%=(const BigInteger &value);

  // унарный минус
  BigInteger operator-();

  // префиксный инкремент
  BigInteger &operator++();

  // постфиксный инкремент
  BigInteger operator++(int);

  // префиксный декремент
  BigInteger &operator--();

  // постфиксный декремент
  BigInteger operator--(int);

  // ==
  bool operator==(const BigInteger &) const;

  // !=
  bool operator!=(const BigInteger &) const;

  // <
  bool operator<(const BigInteger &) const;

  // >
  bool operator>(const BigInteger &) const;

  // <=
  bool operator<=(const BigInteger &) const;

  // >=
  bool operator>=(const BigInteger &) const;

  // вывод
  friend std::ostream &operator<<(std::ostream &, const BigInteger &);

  // ввод
  friend std::istream &operator>>(std::istream &, BigInteger &);

  // метод toString()
  const string toString() const;

  friend BigInteger GetSubtraction(const BigInteger &, const BigInteger &);

  friend BigInteger GetAddition(const BigInteger &, const BigInteger &);

  friend BigInteger GetAbs(const BigInteger &);

 private:
  vector<short int> number_;
  short int sign_;
};

BigInteger::BigInteger() {
  sign_ = 1;
}

int GetSign(const int &value) {
  if (value >= 0) {
    return 1;
  }
  return -1;
}

BigInteger::BigInteger(const int &value) {
  sign_ = GetSign(value);

  int abs_value = std::abs(value);
  if (!abs_value) {
    number_.emplace_back(0);
    sign_ = 1;
  }
  while (abs_value) {
    number_.emplace_back(abs_value % system_number);
    abs_value /= system_number;
  }
}

BigInteger::BigInteger(const string &str) {
  sign_ = 1;
  for (int i = int(str.length() - 1); i > 0; --i)
    number_.emplace_back(str[i] - ascii_first_digit_index);
  if (str[0] == '-') {
    sign_ = -1;
  } else {
    number_.emplace_back(str[0] - ascii_first_digit_index);
  }
}

BigInteger::~BigInteger() {};

const string BigInteger::toString() const {
  string str = "";
  if (number_[0] == 0 && number_.size() == 1) {
    str = "0";
    return str;
  }
  if (sign_ == -1) {
    str = "-";
  }
  for (int i = 0; i < int(number_.size()); ++i) {
    str += (number_[number_.size() - i - 1] + ascii_first_digit_index);
  }

  return str;
}

BigInteger GetAbs(const BigInteger &value) {
  BigInteger abs_value = value;
  abs_value.sign_ = 1;
  return abs_value;
}

BigInteger GetSubtraction(const BigInteger &first_value,
                          const BigInteger &second_value) {
  BigInteger subtract = first_value;
  int new_length = max(first_value.number_.size(),
                       second_value.number_.size());
  BigInteger copy_second_val = second_value;
  copy_second_val.number_.resize(new_length, 0);
  for (int i = 0; i < new_length; ++i) {
    if (subtract.number_[i] >= copy_second_val.number_[i]) {

      subtract.number_[i] -= copy_second_val.number_[i];

    } else {

      subtract.number_[i] -= (copy_second_val.number_[i] - system_number);
      --subtract.number_[i + 1];
    }
  }

  int zero_index = new_length - 1;

  while (subtract.number_[zero_index] == 0 && subtract.number_.size() > 1) {
    --zero_index;
    subtract.number_.pop_back();
  }

  return subtract;
}

BigInteger GetAddition(const BigInteger &first_value,
                       const BigInteger &second_value) {
  BigInteger sum = first_value;
  int new_length =
          max(first_value.number_.size(), second_value.number_.size());
  for (int i = 0; i < new_length; ++i) {

    if (int(second_value.number_.size()) > i) {
      if (sum.number_[i] + second_value.number_[i] >= system_number) {
        sum.number_[i] =
                (sum.number_[i] + second_value.number_[i]) %
                system_number;

        if (i + 1 == new_length) {
          sum.number_.emplace_back(1);
        } else {
          ++sum.number_[i + 1];
        }

      } else {
        sum.number_[i] += second_value.number_[i];
      }
    } else {
      if (sum.number_[i] >= system_number) {
        sum.number_[i] = sum.number_[i] % system_number;
        if (i + 1 == new_length) {
          sum.number_.emplace_back(1);
        } else {
          ++sum.number_[i + 1];
        }
      }
    }
  }
  return sum;
}

std::ostream &operator<<(std::ostream &out, const BigInteger &value) {
  string str = value.toString();
  out << str;
  return out;
}

std::istream &operator>>(std::istream &in, BigInteger &value) {
  string str;
  in >> str;
  BigInteger val(str);
  value = val;
  return in;
}

bool BigInteger::operator<(const BigInteger &value) const {
  if (sign_ > value.sign_) {
    return false;
  }
  if (sign_ < value.sign_ && (number_[number_.size() - 1] != 0 &&
                              value.number_[value.number_.size() - 1] != 0)) {
    return true;
  }

  if ((number_.size() < value.number_.size() && sign_ == 1) ||
      (number_.size() > value.number_.size() && sign_ == -1)) {
    return true;
  }

  if ((number_.size() > value.number_.size() && sign_ == 1) ||
      (number_.size() < value.number_.size() && sign_ == -1)) {
    return false;
  }

  for (int i = number_.size() - 1; i >= 0; --i) {
    if ((number_[i] < value.number_[i] && sign_ == 1) ||
        (number_[i] > value.number_[i] && sign_ == -1)) {
      return true;
    }
    if ((number_[i] > value.number_[i] && sign_ == 1) ||
        (number_[i] < value.number_[i] && sign_ == -1)) {
      return false;
    }
  }
  return false;
}

bool BigInteger::operator>(const BigInteger &value) const {
  return value < *this;
}

bool BigInteger::operator==(const BigInteger &value) const {
  return (!(*this < value) && !(value < *this));
}

bool BigInteger::operator<=(const BigInteger &value) const {
  return (*this < value || *this == value);
}

bool BigInteger::operator>=(const BigInteger &value) const {
  return (*this > value || *this == value);
}

bool BigInteger::operator!=(const BigInteger &value) const {
  return !(*this == value);
}

BigInteger operator-(const BigInteger &first_value,
                     const BigInteger &second_value) {
  if (first_value.sign_ == 1 && second_value.sign_ == 1) {
    if (first_value > second_value || first_value == second_value) {
      return GetSubtraction(first_value, second_value);
    }
    BigInteger subtract = GetSubtraction(second_value, first_value);
    subtract.sign_ = -1;
    return subtract;
  }

  if (first_value.sign_ == -1 && second_value.sign_ == -1) {
    if (first_value < second_value) {
      BigInteger subtract = GetSubtraction(GetAbs(first_value),
                                           GetAbs(second_value));
      subtract.sign_ = -1;
      return subtract;
    }
    return GetSubtraction(GetAbs(second_value), GetAbs(first_value));
  }
  if (first_value.sign_ == -1) {
    if (GetAbs(first_value) > second_value) {
      BigInteger subtract = GetAddition(GetAbs(first_value),
                                        GetAbs(second_value));
      subtract.sign_ = -1;
      return subtract;
    }
    BigInteger subtract = GetAddition(GetAbs(second_value),
                                      GetAbs(first_value));
    subtract.sign_ = -1;
    return subtract;
  } else {
    if (first_value > GetAbs(second_value)) {
      return GetAddition(first_value, GetAbs(second_value));
    }
    return GetAddition(GetAbs(second_value), first_value);
  }
}

BigInteger operator+(const BigInteger &first_value,
                     const BigInteger &second_value) {
  if (first_value.sign_ == 1 && second_value.sign_ == 1) {
    if (first_value > second_value) {
      return GetAddition(first_value, second_value);
    }
    return GetAddition(second_value, first_value);

  }
  if (first_value.sign_ == -1 && second_value.sign_ == -1) {
    if (first_value < second_value) {
      BigInteger sum = GetAddition(GetAbs(first_value),
                                   GetAbs(second_value));
      sum.sign_ = -1;
      return sum;
    }
    BigInteger sum = GetAddition(GetAbs(second_value), GetAbs(first_value));
    sum.sign_ = -1;
    return sum;
  }
  if (first_value.sign_ == 1) {
    return first_value - GetAbs(second_value);
  }
  return second_value - GetAbs(first_value);
}

BigInteger &BigInteger::operator++() {
  (*this) = (*this) + 1;
  return *this;
}

BigInteger &BigInteger::operator--() {
  (*this) = (*this) - 1;
  return *this;
}

BigInteger BigInteger::operator++(int) {
  BigInteger new_big_int = *this;
  (*this) = (*this) + 1;
  return new_big_int;
}

BigInteger BigInteger::operator--(int) {
  BigInteger new_big_int = *this;
  (*this) = (*this) - 1;
  return new_big_int;
}

BigInteger &BigInteger::operator+=(const BigInteger &value) {
  (*this) = (*this) + value;
  return *this;
}

BigInteger &BigInteger::operator-=(const BigInteger &value) {
  (*this) = (*this) - value;
  return *this;
}

BigInteger BigInteger::operator-() {
  BigInteger new_big_int = *this;
  if (new_big_int != 0) {
    new_big_int.sign_ *= -1;
  }
  return new_big_int;
}

BigInteger operator*(const BigInteger &first_value,
                     const BigInteger &second_value) {
  BigInteger product;
  if (first_value == 0 || second_value == 0) {
    product = 0;
    return product;
  }

  int number_count = first_value.number_.size() + second_value.number_.size()
                     + 1;
  product.sign_ = first_value.sign_ * second_value.sign_;
  product.number_.resize(number_count, 0);
  if (first_value.number_.size() < min_length_for_karatsuba) {
    for (int i = 0; i < int(first_value.number_.size()); ++i) {
      for (int j = 0; j < int(second_value.number_.size()); ++j) {
        if (product.number_[i + j] + first_value.number_[i] *
                                     second_value.number_[j] >=
            system_number) {
          product.number_[i + j + 1] += (product.number_[i + j] +
                                         first_value.number_[i] *
                                         second_value.number_[j]) /
                                        system_number;
          product.number_[i + j] = (product.number_[i + j] +
                                    first_value.number_[i] *
                                    second_value.number_[j]) %
                                   system_number;
        } else {
          product.number_[i + j] +=
                  first_value.number_[i] * second_value.number_[j];
        }
      }
    }
  } else {
    int count = max(first_value.number_.size(),
                    second_value.number_.size());
    BigInteger copy_first_value = first_value;
    copy_first_value.number_.resize(count, 0);
    BigInteger copy_second_value = second_value;
    copy_second_value.number_.resize(count, 0);

    BigInteger young_part_first_val;
    for (int i = 0; i < (count + 1) / 2; ++i) {
      young_part_first_val.number_.emplace_back(
              copy_first_value.number_[i]);
    }

    BigInteger high_part_first_val;
    for (int i = (count + 1) / 2; i < count; ++i) {
      high_part_first_val.number_.emplace_back(
              copy_first_value.number_[i]);
    }

    BigInteger young_part_second_val;
    for (int i = 0; i < (count + 1) / 2; ++i) {
      young_part_second_val.number_.emplace_back(
              copy_second_value.number_[i]);
    }

    BigInteger high_part_second_val;
    for (int i = (count + 1) / 2; i < count; ++i) {
      high_part_second_val.number_.emplace_back(
              copy_second_value.number_[i]);
    }

    BigInteger sum_of_first_parts =
            young_part_first_val + high_part_first_val;

    BigInteger sum_of_second_parts =
            young_part_second_val + high_part_second_val;

    BigInteger product_of_sums_of_parts =
            sum_of_first_parts * sum_of_second_parts;

    BigInteger product_of_young_parts =
            young_part_first_val * young_part_second_val;

    BigInteger product_of_high_parts =
            high_part_first_val * high_part_second_val;

    BigInteger sum_of_middle_terms =
            product_of_sums_of_parts -
            (product_of_young_parts + product_of_high_parts);


    for (int i = 0; i < int(product_of_young_parts.number_.size()); ++i) {
      product.number_[i] = product_of_young_parts.number_[i];
    }

    int cur_index = (count + 1) / 2;
    for (int i = 0; i < int(sum_of_middle_terms.number_.size()); ++i) {
      if (product.number_[cur_index] +
          sum_of_middle_terms.number_[i] >= system_number) {

        product.number_[cur_index + 1] +=
                (product.number_[cur_index] +
                 sum_of_middle_terms.number_[i]) / system_number;

        product.number_[cur_index] =
                (product.number_[cur_index] +
                 sum_of_middle_terms.number_[i]) % system_number;
      } else {
        product.number_[cur_index] += sum_of_middle_terms.number_[i];
      }
      ++cur_index;
    }

    cur_index = ((count + 1) / 2) * 2;
    for (int i = 0; i < int(product_of_high_parts.number_.size()); ++i) {

      if (product.number_[cur_index] +
          product_of_high_parts.number_[i] >= system_number) {
        product.number_[cur_index + 1] +=
                (product.number_[cur_index] +
                 product_of_high_parts.number_[i]) / system_number;

        product.number_[cur_index] =
                (product.number_[cur_index] +
                 product_of_high_parts.number_[i]) % system_number;
      } else {
        product.number_[cur_index] +=
                product_of_high_parts.number_[i];
      }
      ++cur_index;
    }
  }

  int zero_index = product.number_.size() - 1;

  while (product.number_[zero_index] == 0 && product.number_.size() > 1) {
    --zero_index;
    product.number_.pop_back();
  }
  return product;
}

BigInteger operator/(const BigInteger &first_value,
                     const BigInteger &second_value) {
  BigInteger result;
  if (GetAbs(first_value) < GetAbs(second_value)) {
    result = 0;
    return result;
  }
  BigInteger abs_second_value = second_value;
  abs_second_value.sign_ = 1;

  result.number_.resize(first_value.number_.size());
  BigInteger cur_value;

  for (int i = int(first_value.number_.size()) - 1; i >= 0; --i) {
    cur_value.number_.insert(cur_value.number_.begin(), 0);
    int zero_index = cur_value.number_.size() - 1;

    while (cur_value.number_[zero_index] == 0 &&
           cur_value.number_.size() > 1) {
      --zero_index;
      cur_value.number_.pop_back();
    }

    cur_value.number_[0] = first_value.number_[i];
    int x = 0;
    int left = 0;
    int right = system_number - 1;
    while (left <= right) {
      int m = (left + right) / 2;
      BigInteger cur(0);
      cur = abs_second_value * m;
      if (cur <= cur_value) {
        x = m;
        left = m + 1;
      } else
        right = m - 1;
    }
    result.number_[i] = x;
    cur_value -= abs_second_value * x;

  }
  result.sign_ = first_value.sign_ * second_value.sign_;

  int zero_index = result.number_.size() - 1;

  while (result.number_[zero_index] == 0 && result.number_.size() > 1) {
    --zero_index;
    result.number_.pop_back();
  }
  return result;
}

BigInteger operator%(const BigInteger &first_value,
                     const BigInteger &second_value) {
  return first_value - (first_value / second_value) * second_value;
}

BigInteger &BigInteger::operator*=(const BigInteger &value) {
  (*this) = (*this) * value;
  return *this;
}

BigInteger &BigInteger::operator/=(const BigInteger &value) {
  (*this) = (*this) / value;
  return *this;
}

BigInteger &BigInteger::operator%=(const BigInteger &value) {
  (*this) = (*this) % value;
  return *this;
}

BigInteger::operator bool() {
  return *this != 0;
}