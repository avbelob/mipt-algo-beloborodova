#include <iostream>
#include "biginteger.h"

int main() {
  BigInteger c1("-34344375748787447487859489");
  BigInteger c2("-286386327523723827823723");
  BigInteger c3("-22729292982");
  BigInteger c4("-7453");
  BigInteger c5("9835759641790079721124660979628653224409214150529");

  std::cout << c5 / c4  << "\n";
  std::cout << (c1 * c2 - c3) / c4 <<"\n";

/*
 (-34344375748787447487859489 * -286386327523723827823723 - (-22729292982)) / -7453
 */


  /*for (int i = -46340; i <= 46340; ++i) {
    std::cout << i << "\n";
    for (int j = -46340; j <= 46340; ++j) {
      BigInteger b1 = i;
      BigInteger b2 = j;
      BigInteger b3;
      b3 = b1 + b2;

      if((i == j) != (b1 == b2)) {
        std::cout << "==\n";
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2;
        return 0;
      }

      if((i != j) != (b1 != b2)) {
        std::cout << "!=\n";
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2;
        return 0;
      }

      if((i < j) != (b1 < b2)) {
        std::cout << "<\n";
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2;
        return 0;
      }

      if((i > j) != (b1 > b2)) {
        std::cout << ">\n";
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2;
        return 0;
      }

      if((i <= j) != (b1 <= b2)) {
        std::cout << "<=\n";
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2;
        return 0;
      }

      if((i >= j) != (b1 >= b2)) {
        std::cout << ">=\n";
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2;
        return 0;
      }
      if (!(b3 == BigInteger(i + j))) {
        std::cout << i << " " << j << "\n";
        std::cout << b1 + b2 << " " << i + j;
        return 0;
      }
      b3 = b1 - b2;
      if (!(b3 == BigInteger(i - j))) {
        std::cout << i << " " << j << "\n";
        std::cout << b1 - b2 << " " << i - j;
        return 0;
      }
      b3 = b1 * b2;
      if (!(b3 == BigInteger(i * j))) {
        std::cout << i << " " << j << "\n";
        std::cout << b1 << " " << b2 << "\n";
        std::cout << b3  << " " << BigInteger(i * j);
        return 0;
      }
      if (j != 0 && !((b1 / b2) == BigInteger(i / j))) {
        std::cout << i << " " << j << "\n";
        std::cout << b1 / b2 << " " << i / j;
        return 0;
      }
      if (j != 0 && !((b1 % b2) == BigInteger(i % j))) {
        std::cout << i << " " << j << "\n";
        std::cout << b1 % b2 << " " << i % j;
        return 0;
      }

    }
  }
  std::cout<<":)";*/
}
