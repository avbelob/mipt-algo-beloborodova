#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;
using std::swap;
using std::reverse;

enum {
  INF = INT64_MAX
};

const double epsilon = 0.00000000001;

double double_abs(const double x) {
  if(x >= 0) {
    return x;
  }
  return -x;
}

struct Vector2D {
  double x;
  double y;

  Vector2D(const double &x, const double &y);

  friend Vector2D operator+(const Vector2D &first_vector,
                            const Vector2D &second_vector) {
    Vector2D cur_vector(first_vector.x + second_vector.x,
                        first_vector.y + second_vector.y);
    return cur_vector;
  }

  friend Vector2D operator-(const Vector2D &first_vector,
                            const Vector2D &second_vector) {
    Vector2D cur_vector(first_vector.x - second_vector.x,
                        first_vector.y - second_vector.y);
    return cur_vector;
  }
};

Vector2D::Vector2D(const double &x, const double &y) : x(x), y(y) {}

bool IsPolarAngleLess(const Vector2D &first_vector,
                      const Vector2D &second_vector) {
  if (first_vector.x < 0 && second_vector.x > 0) {
    return false;
  }
  if (first_vector.x > 0 && second_vector.x < 0) {
    return true;
  }

  double first_tg;
  double second_tg;

  if (double_abs(first_vector.x) >= epsilon) {
    first_tg = first_vector.y / first_vector.x;
  } else {
    first_tg = INF;
  }


  if (double_abs(second_vector.x) >= epsilon) {
    second_tg = second_vector.y / second_vector.x;
  } else {
    second_tg = INF;
  }

  if (double_abs(second_vector.x) < epsilon) {
    if (second_vector.y < 0) {
      return (double_abs(first_tg - second_tg) > epsilon);
    }
    return (first_vector.x > 0);

  } else if (double_abs(second_vector.x) < epsilon) {
    return first_vector.y >= 0 && second_vector.x > 0;
  }

  return double_abs(first_tg - second_tg) >= epsilon && first_tg < second_tg;
}

bool IsSignsDifferent(const double &first, const double &second) {
  return first * second < 0;
}

struct Polygon {

  vector<Vector2D> polygon_vector;

  Polygon() = default;

  explicit Polygon(const vector<Vector2D> &new_vector);

  void Normalize();

  bool IsContainsZero() const;

};

Polygon::Polygon(const vector<Vector2D> &new_vector) :
        polygon_vector(new_vector) {}

void Polygon::Normalize() {
  vector<Vector2D> new_polygon_vector;

  int min = 0;

  for (int i = 1; i < polygon_vector.size(); ++i) {
    if (((double_abs(polygon_vector[i].x - polygon_vector[min].x) < epsilon) &&
         (polygon_vector[min].y > polygon_vector[i].y)) ||
        (polygon_vector[min].x > polygon_vector[i].x)) {
      min = i;
    }
  }

  for (int i = 0; i < polygon_vector.size(); ++i) {
    Vector2D cur_vector = polygon_vector[(min + i + 1) % polygon_vector.size()];
    new_polygon_vector.emplace_back(cur_vector);
  }

  reverse(new_polygon_vector.begin(), new_polygon_vector.end());

  polygon_vector.clear();
  swap(polygon_vector, new_polygon_vector);

}

bool Polygon::IsContainsZero() const {

  vector<double> tmp;

  if (IsSignsDifferent(polygon_vector[0].y,
                       polygon_vector[polygon_vector.size() - 1].y)) {
    tmp.emplace_back((polygon_vector[0].x +
                      polygon_vector[polygon_vector.size() - 1].x) / 2);
  }

  for (int i = 0; i < polygon_vector.size() - 1; ++i) {
    if (IsSignsDifferent(polygon_vector[i].y,
                         polygon_vector[i + 1].y)) {
      tmp.emplace_back((polygon_vector[i].x + polygon_vector[i + 1].x) / 2);
    }
  }

  for (Vector2D vector : polygon_vector) {
    if (double_abs(vector.y) < epsilon) {
      tmp.emplace_back(vector.x);
    }
  }

  if (tmp.empty()) {
    return false;
  }

  double min_element = tmp[0];
  double max_element = tmp[0];
  for(int i = 1; i < tmp.size(); ++i) {
    if(min_element > tmp[i]){
      min_element = tmp[i];
    }
    if(max_element < tmp[i]){
      max_element = tmp[i];
    }
  }


  return IsSignsDifferent(max_element, min_element) ||
  double_abs(min_element) < epsilon || double_abs(max_element) < epsilon;

}

Polygon GetMinkowskiSum(Polygon first_polygon, Polygon second_polygon) {
  Polygon minkowski_sum;

  first_polygon.polygon_vector.emplace_back(first_polygon.polygon_vector[0]);
  second_polygon.polygon_vector.emplace_back(second_polygon.polygon_vector[0]);

  int i = 0;
  int j = 0;

  while (i + 1 < (first_polygon.polygon_vector.size()) &&
         (j + 1 < second_polygon.polygon_vector.size())) {
    minkowski_sum.polygon_vector.emplace_back(
            first_polygon.polygon_vector[i] + second_polygon.polygon_vector[j]);

    if (IsPolarAngleLess((first_polygon.polygon_vector[i + 1] -
                          first_polygon.polygon_vector[i]),
                         (second_polygon.polygon_vector[j + 1] -
                          second_polygon.polygon_vector[j]))) {
      ++i;
    } else if (IsPolarAngleLess((second_polygon.polygon_vector[j + 1] -
                                 second_polygon.polygon_vector[j]),
                                (first_polygon.polygon_vector[i + 1] -
                                 first_polygon.polygon_vector[i]))) {
      ++j;
    } else {
      ++i;
      ++j;
    }
  }

  return minkowski_sum;
}

bool CheckIntersection(const Polygon &first_polygon,
                       const Polygon &second_polygon) {

  Polygon res_polygon = GetMinkowskiSum(first_polygon, second_polygon);

  return res_polygon.IsContainsZero();
}

int main() {
  int first_polygon_point_count;
  int second_polygon_point_count;
  vector<Vector2D> new_polygon_vector;
  double x;
  double y;

  std::cin >> first_polygon_point_count;

  for (int i = 0; i < first_polygon_point_count; ++i) {
    std::cin >> x >> y;
    Vector2D cur_vector(x, y);
    new_polygon_vector.emplace_back(cur_vector);
  }

  Polygon first_polygon(new_polygon_vector);

  new_polygon_vector.clear();
  std::cin >> second_polygon_point_count;

  for (int i = 0; i < second_polygon_point_count; ++i) {
    std::cin >> x >> y;
    Vector2D cur_vector(-x, -y);
    new_polygon_vector.emplace_back(cur_vector);
  }

  Polygon second_polygon(new_polygon_vector);

  first_polygon.Normalize();
  second_polygon.Normalize();

  if (CheckIntersection(first_polygon, second_polygon)) {
    std::cout << "YES";
  } else {
    std::cout << "NO";
  }
}