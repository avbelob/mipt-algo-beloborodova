#include <cmath>
#include <iostream>
#include <iomanip>

using std::min;
using std::pair;
using std::make_pair;

double min(const double &a, const double &b, const double &c, const double &d) {
  return min(min(a, b), min(c, d));
}

struct Vector3D {
  double x;
  double y;
  double z;

  explicit Vector3D(const double &x, const double &y, const double &z) :
          x(x), y(y), z(z) {}
};

struct Segment3D {
  Vector3D start;
  Vector3D direction;

  explicit Segment3D(const Vector3D &start, const Vector3D &direction) :
          start(start), direction(direction) {}
};

Vector3D GetSegmentEnd(const Segment3D &segment) {
  Vector3D vector_end(segment.start.x + segment.direction.x,
                      segment.start.y + segment.direction.y,
                      segment.start.z + segment.direction.z);
  return vector_end;
}

double GetScalarProd(const Vector3D &first_vector,
                     const Vector3D &second_vector) {
  return (first_vector.x * second_vector.x) +
         (first_vector.y * second_vector.y) +
         (first_vector.z * second_vector.z);
};

Vector3D GetVectorProd(const Vector3D &first_vector,
                       const Vector3D &second_vector) {
  double x = first_vector.y * second_vector.z -
             first_vector.z * second_vector.y;

  double y = first_vector.x * second_vector.z -
             first_vector.z * second_vector.x;

  double z = first_vector.x * second_vector.y -
             first_vector.y * second_vector.x;

  Vector3D vector_product(x, y, z);
  return vector_product;
};

bool IsCollinear(const Vector3D &first_vector, const Vector3D &second_vector) {

  Vector3D vector_product = GetVectorProd(first_vector, second_vector);

  return vector_product.x == 0 &&
         vector_product.y == 0 &&
         vector_product.z == 0;
};

pair<Segment3D, bool> GetMinSegmentBetweenLines(const Segment3D &first_segment,
                                                const Segment3D
                                                &second_segment) {

  Vector3D vector_between_starts(
          second_segment.start.x - first_segment.start.x,
          second_segment.start.y - first_segment.start.y,
          second_segment.start.z - first_segment.start.z);

  double first_direct_square = GetScalarProd(first_segment.direction,
                                             first_segment.direction);
  double second_direct_square = GetScalarProd(second_segment.direction,
                                              second_segment.direction);
  double direct_scalar_product = GetScalarProd(first_segment.direction,
                                               second_segment.direction);
  double first_scalar_product = GetScalarProd(first_segment.direction,
                                              vector_between_starts);
  double second_scalar_product = GetScalarProd(second_segment.direction,
                                               vector_between_starts);

  double first_param = (second_direct_square * first_scalar_product -
                        direct_scalar_product * second_scalar_product) /
                       (first_direct_square * second_direct_square -
                        direct_scalar_product * direct_scalar_product);

  double second_param = (direct_scalar_product * first_scalar_product -
                         first_direct_square * second_scalar_product) /
                        (first_direct_square * second_direct_square -
                         direct_scalar_product * direct_scalar_product);

  Vector3D start_vector(first_segment.start.x +
                        first_param * first_segment.direction.x,
                        first_segment.start.y +
                        first_param * first_segment.direction.y,
                        first_segment.start.z +
                        first_param * first_segment.direction.z);

  Vector3D end_vector(second_segment.start.x +
                      second_param * second_segment.direction.x,
                      second_segment.start.y +
                      second_param * second_segment.direction.y,
                      second_segment.start.z +
                      second_param * second_segment.direction.z);

  Vector3D direction_vector(end_vector.x - start_vector.x,
                            end_vector.y - start_vector.y,
                            end_vector.z - start_vector.z);

  return make_pair(Segment3D(start_vector, direction_vector),
                   (first_param >= 0 && first_param <= 1 &&
                    second_param >= 0 && second_param <= 1));
}

double GetDistBetweenVectorsEnd(const Vector3D &first_vector,
                                const Vector3D &second_vector) {
  return sqrt(pow(first_vector.x - second_vector.x, 2) +
              pow(first_vector.y - second_vector.y, 2) +
              pow(first_vector.z - second_vector.z, 2));
}

double GetDistBetweenPointAndSegment(const Vector3D &point,
                                     const Segment3D &segment) {
  Vector3D help_vector(point.x - segment.start.x, point.y - segment.start.y,
                       point.z - segment.start.z);
  if (GetScalarProd(segment.direction, help_vector) <= 0) {
    return GetDistBetweenVectorsEnd(point, segment.start);
  }
  if (GetScalarProd(segment.direction, segment.direction) <=
      GetScalarProd(segment.direction, help_vector)) {
    Vector3D segment_end = GetSegmentEnd(segment);
    return GetDistBetweenVectorsEnd(point, segment_end);
  }

  double param = GetScalarProd(segment.direction, help_vector) /
                 GetScalarProd(segment.direction, segment.direction);
  Vector3D min_segment_end(segment.start.x + param * segment.direction.x,
                           segment.start.y + param * segment.direction.y,
                           segment.start.z + param * segment.direction.z);
  return GetDistBetweenVectorsEnd(point, min_segment_end);
}

double GetSegmentLength(const Segment3D &segment) {
  return sqrt(pow(segment.direction.x, 2) +
              pow(segment.direction.y, 2) +
              pow(segment.direction.z, 2));
}

double GetDistBetweenSegments(const Segment3D &first_segment,
                              const Segment3D &second_segment) {

  if (!IsCollinear(first_segment.direction, second_segment.direction)) {
    pair<Segment3D, bool> min_segment_between_lines =
            GetMinSegmentBetweenLines(first_segment, second_segment);

    if (min_segment_between_lines.second) {
      return GetSegmentLength(min_segment_between_lines.first);
    }
  }

  Vector3D first_segment_end = GetSegmentEnd(first_segment);
  Vector3D second_segment_end = GetSegmentEnd(second_segment);
  double min_dist_between_segments = min(
          GetDistBetweenPointAndSegment(first_segment.start, second_segment),
          GetDistBetweenPointAndSegment(first_segment_end, second_segment),
          GetDistBetweenPointAndSegment(second_segment.start, first_segment),
          GetDistBetweenPointAndSegment(second_segment_end, first_segment));
  return min_dist_between_segments;
}

int main() {
  double x, y, z;

  std::cin >> x >> y >> z;
  Vector3D first_start(x, y, z);
  std::cin >> x >> y >> z;
  Vector3D first_direction(x - first_start.x, y - first_start.y,
                           z - first_start.z);
  Segment3D first_segment(first_start, first_direction);

  std::cin >> x >> y >> z;
  Vector3D second_start(x, y, z);
  std::cin >> x >> y >> z;
  Vector3D second_direction(x - second_start.x, y - second_start.y,
                            z - second_start.z);
  Segment3D second_segment(second_start, second_direction);

  std::cout << std::fixed << std::setprecision(10) <<
            GetDistBetweenSegments(first_segment, second_segment);
}