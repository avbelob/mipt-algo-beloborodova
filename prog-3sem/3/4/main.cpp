#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <cmath>
#include <iomanip>

using std::set;
using std::swap;
using std::sort;
using std::vector;
using std::max;
using std::min;

typedef std::pair<int, int> Edge;

enum {
  INF = INT64_MAX,
  diff_situations_count = 6
};

const double angle_to_rotate = 1e-6;

struct Point3D {
  double x;
  double y;
  double z;
  int index;

  Point3D *next_point;
  Point3D *prev_point;

  explicit Point3D(const int &index, const double &x,
                   const double &y, const double &z);

  bool PerformChange();

  friend bool operator<(const Point3D &first_point,
                        const Point3D &second_point) {
    return first_point.x < second_point.x;
  }

  friend bool operator==(const Point3D &first_point,
                         const Point3D &second_point) {
    return (first_point.x == second_point.x) &&
           (first_point.y == second_point.y) &&
           (first_point.z == second_point.z);
  }
};

Point3D::Point3D(const int &index, const double &x,
                 const double &y, const double &z) :
        x(x), y(y), z(z), index(index), next_point(nullptr),
        prev_point(nullptr) {}

bool Point3D::PerformChange() {
  if (this != this->prev_point->next_point) {
    this->next_point->prev_point = this;
    this->prev_point->next_point = this;
    return true;
  }

  this->next_point->prev_point = prev_point;
  this->prev_point->next_point = next_point;
  return false;
}

void rotate(Point3D &point, const double angle) {
  double new_x, new_y, new_z;
  new_x = point.x * cos(angle) - point.y * sin(angle);
  new_y = point.x * sin(angle) + point.y * cos(angle);
  point.y = new_y;
  point.x = new_x;

  new_x = point.x * cos(angle) - point.z * sin(angle);
  new_z = point.x * sin(angle) + point.z * cos(angle);
  point.z = new_z;
  point.x = new_x;

  new_z = point.z * cos(angle) - point.y * sin(angle);
  new_y = point.z * sin(angle) + point.y * cos(angle);
  point.y = new_y;
  point.z = new_z;
}

bool IsOrientationRight(const Point3D *first_point, const Point3D *second_point,
                        const Point3D *third_point) {

  return (first_point == nullptr) ||
         (second_point == nullptr) ||
         (third_point == nullptr) ||
         (second_point->x - first_point->x) *
         (third_point->y - second_point->y) -
         (second_point->y - first_point->y) *
         (third_point->x - second_point->x) > 0;

}

struct Face {
  int first_point_index;
  int second_point_index;
  int third_point_index;

  explicit Face(const int &first_index, const int &second_index,
                const int &third_index);

  void FixOrder();

  friend bool operator<(const Face &first_face, const Face &second_face) {

    return (first_face.first_point_index < second_face.first_point_index) ||
           ((first_face.first_point_index ==
             second_face.first_point_index) &&
            (first_face.second_point_index <
             second_face.second_point_index)) ||
           ((first_face.first_point_index ==
             second_face.first_point_index) &&
            (first_face.second_point_index ==
             second_face.second_point_index) &&
            (first_face.third_point_index < second_face.third_point_index));

  }

};

Face::Face(const int &first_index, const int &second_index,
           const int &third_index) :
        first_point_index(first_index), second_point_index(second_index),
        third_point_index(third_index) {}

void Face::FixOrder() {
  if ((first_point_index > second_point_index) &&
      (second_point_index < third_point_index)) {

    swap(first_point_index, second_point_index);
    swap(second_point_index, third_point_index);

    return;
  }

  if ((first_point_index > third_point_index) &&
      (second_point_index > third_point_index)) {

    swap(second_point_index, third_point_index);
    swap(first_point_index, second_point_index);
  }
}

class ConvexHull {

 public:
  explicit ConvexHull(vector<Point3D> &points);

  vector<Face> GetHullVector() const;

  void FixOrder();

 private:

  vector<Face> hull_vector;

  vector<Point3D *> BuildRecursHull(vector<Point3D> &points,
                                    const int left_border,
                                    const int right_border);

  vector<Point3D *> MergeHulls(vector<Point3D> &points,
                               const vector<Point3D *> first_hull,
                               const vector<Point3D *> second_hull,
                               const int middle);
};

ConvexHull::ConvexHull(vector<Point3D> &points) {
  sort(points.begin(), points.end());
  int left_border = 0;
  int right_border = points.size();
  vector<Point3D *> points_vector =
          BuildRecursHull(points, left_border, right_border);

  for (Point3D *cur_point : points_vector) {
    Face cur_face(cur_point->prev_point->index, cur_point->index,
                  cur_point->next_point->index);

    if (!(cur_point->PerformChange())) {
      swap(cur_face.first_point_index, cur_face.second_point_index);
    }
    hull_vector.emplace_back(cur_face);
  }

  for (Point3D &cur_point : points) {
    cur_point.prev_point = nullptr;
    cur_point.next_point = nullptr;
    cur_point.z = -cur_point.z;
  }
  points_vector = BuildRecursHull(points, left_border, right_border);

  for (Point3D *cur_point : points_vector) {
    Face current_face(cur_point->prev_point->index, cur_point->index,
                      cur_point->next_point->index);

    if (cur_point->PerformChange()) {
      swap(current_face.first_point_index,
           current_face.second_point_index);
    }
    hull_vector.emplace_back(current_face);
  }
}

vector<Face> ConvexHull::GetHullVector() const {
  return hull_vector;
}

void ConvexHull::FixOrder() {
  for (Face &current_face : hull_vector) {
    current_face.FixOrder();
  }
  sort(hull_vector.begin(), hull_vector.end());
}

vector<Point3D *> ConvexHull::BuildRecursHull(vector<Point3D> &points,
                                              const int left_border,
                                              const int right_border) {

  if ((right_border - left_border) <= 1) {
    return vector<Point3D *>();
  }

  int middle = (left_border + right_border) / 2;
  vector<Point3D *> first_hull = BuildRecursHull(points, left_border, middle);
  vector<Point3D *> second_hull = BuildRecursHull(points, middle,
                                                  right_border);

  return MergeHulls(points, first_hull, second_hull, middle);
}

void GetReferenceLine(Point3D *&u_point, Point3D *&v_point) {
  while (true) {
    if (IsOrientationRight(u_point, v_point, v_point->next_point)) {
      if (IsOrientationRight(u_point->prev_point, u_point, v_point)) {
        return;
      }
      u_point = u_point->prev_point;
    } else {
      v_point = v_point->next_point;
    }
  }
}

double GetParamT(const Point3D *first_point, const Point3D *second_point,
                 const Point3D *third_point) {
  if ((first_point == nullptr) || (second_point == nullptr) ||
      (third_point == nullptr)) {
    return INF;
  } else {

    return ((second_point->x - first_point->x) *
            (third_point->z - second_point->z) -
            (second_point->z - first_point->z) *
            (third_point->x - second_point->x)) /
           ((second_point->x - first_point->x) *
            (third_point->y - second_point->y) -
            (second_point->y - first_point->y) *
            (third_point->x - second_point->x));
  }
}

vector<Point3D *> GetCurHull(const vector<Point3D *> first_hull,
                             const vector<Point3D *> second_hull,
                             Point3D* &u_point, Point3D* &v_point) {
  vector<Point3D *> hulls;
  int first_point_index = 0;
  int second_point_index = 0;

  double current_t = (-1) * INF;
  while (true) {
    vector<double> next_t(diff_situations_count, INF);

    Point3D *left = nullptr;
    Point3D *right = nullptr;

    if (first_point_index < first_hull.size()) {
      left = first_hull[first_point_index];
      next_t[0] = GetParamT(left->prev_point, left, left->next_point);
    }

    if (second_point_index < second_hull.size()) {
      right = second_hull[second_point_index];
      next_t[1] = GetParamT(right->prev_point, right, right->next_point);
    }

    next_t[2] = GetParamT(u_point, v_point, v_point->next_point);
    next_t[3] = GetParamT(u_point, v_point->prev_point, v_point);
    next_t[4] = GetParamT(u_point->prev_point, u_point, v_point);
    next_t[5] = GetParamT(u_point, u_point->next_point, v_point);

    int min_t_index = -1;
    double min_t = INF;
    for (int i = 0; i < diff_situations_count; ++i) {
      if ((next_t[i] < min_t) && (next_t[i] > current_t)) {
        min_t_index = i;
        min_t = next_t[i];
      }
    }

    if ((min_t_index == -1) || (min_t == INF)) {
      break;
    }

    switch (min_t_index) {
      case 0:
        if ((u_point->x) > left->x) {
          hulls.emplace_back(left);
        }
        ++first_point_index;
        left->PerformChange();
        break;

      case 1:
        if ((v_point->x) < (right->x)) {
          hulls.emplace_back(right);
        }
        ++second_point_index;
        right->PerformChange();
        break;

      case 2:
        hulls.emplace_back(v_point);
        v_point = v_point->next_point;
        break;

      case 3:
        v_point = v_point->prev_point;
        hulls.emplace_back(v_point);
        break;

      case 4:
        hulls.emplace_back(u_point);
        u_point = u_point->prev_point;
        break;

      case 5:
        u_point = u_point->next_point;
        hulls.emplace_back(u_point);
        break;

      default:
        break;
    }
    current_t = min_t;
  }

  return hulls;
}

vector<Point3D *> ConvexHull::MergeHulls(vector<Point3D> &points,
                                         const vector<Point3D *> first_hull,
                                         const vector<Point3D *> second_hull,
                                         const int middle) {

  Point3D *u_point = &points[middle - 1];
  Point3D *v_point = &points[middle];

  GetReferenceLine(u_point, v_point);
  vector<Point3D *> hulls = GetCurHull(first_hull, second_hull, u_point,
                                       v_point);

  u_point->next_point = v_point;
  v_point->prev_point = u_point;

  for (int i = hulls.size() - 1; i >= 0; --i) {
    if ((v_point->x > hulls[i]->x) && (u_point->x < hulls[i]->x)) {

      u_point->next_point = v_point->prev_point = hulls[i];
      hulls[i]->next_point = v_point;
      hulls[i]->prev_point = u_point;

      if (points[middle - 1].x < hulls[i]->x) {
        v_point = hulls[i];
      } else {
        u_point = hulls[i];
      }

    } else {
      hulls[i]->PerformChange();
      if (hulls[i] == u_point) {
        u_point = u_point->prev_point;
      }
      if (v_point == hulls[i]) {
        v_point = v_point->next_point;
      }

    }
  }
  return hulls;
}

set<Edge> GetDelaunayTriangulation(vector<Point3D> &points) {
  ConvexHull convex_hull(points);
  convex_hull.FixOrder();
  vector<Face> hull_vector = convex_hull.GetHullVector();
  set<Edge> edges;

  for (int i = 0; i < hull_vector.size(); ++i) {
    Face cur_face = hull_vector[i];
    edges.emplace(
            min(cur_face.first_point_index, cur_face.second_point_index),
            max(cur_face.first_point_index, cur_face.second_point_index));

    edges.emplace(
            min(cur_face.first_point_index, cur_face.third_point_index),
            max(cur_face.first_point_index, cur_face.third_point_index));

    edges.emplace(
            min(cur_face.third_point_index, cur_face.second_point_index),
            max(cur_face.third_point_index, cur_face.second_point_index));
  }
  return edges;
}

bool IsOrientationRight(const Point3D &first_point, const Point3D &second_point,
                        const Point3D &third_point) {

  return first_point.x * (second_point.y - third_point.y) +
         second_point.x * (third_point.y - first_point.y) +
         third_point.x * (first_point.y - second_point.y) < 0;

}

bool IsOrientationLeft(const Point3D &first_point, const Point3D &second_point,
                       const Point3D &third_point) {

  return first_point.x * (second_point.y - third_point.y) +
         second_point.x * (third_point.y - first_point.y) +
         third_point.x * (first_point.y - second_point.y) > 0;

}

vector<Point3D> GetContexHull(vector<Point3D> &points) {
  vector<Point3D> contex_hull;
  if (points.size() <= 2) {
    return contex_hull;
  }
  sort(points.begin(), points.end());
  Point3D start = points[0];
  Point3D end = points.back();
  vector<Point3D> up, down;
  up.emplace_back(start);
  down.emplace_back(start);
  for (int i = 1; i < points.size(); ++i) {
    if (i == points.size() - 1 ||
        IsOrientationRight(start, points[i], end)) {
      while (up.size() >= 2 &&
             !IsOrientationRight(up[up.size() - 2], up[up.size() - 1],
                                 points[i])) {
        up.pop_back();
      }
      up.emplace_back(points[i]);
    }
    if (i == points.size() - 1 ||
        IsOrientationLeft(start, points[i], end)) {
      while (down.size() >= 2 &&
             !IsOrientationLeft(down[down.size() - 2],
                                down[down.size() - 1],
                                points[i])) {
        down.pop_back();
      }
      down.emplace_back(points[i]);
    }
  }

  for (int i = 0; i < up.size(); ++i) {
    contex_hull.push_back(up[i]);
  }
  for (int i = down.size() - 2; i > 0; --i) {
    contex_hull.push_back(down[i]);
  }


  return contex_hull;
}

double GetAverageSidesInVoronoiPolygons(vector<Point3D> &points) {
  set<Edge> edges = GetDelaunayTriangulation(points);
  vector<int> edge_degree(points.size(), 0);
  vector<bool> is_on_border(points.size(), false);
  double polygons_count = 0;
  double edges_count = 0;

  for (const Edge &edge : edges) {
    ++edge_degree[edge.first];
    ++edge_degree[edge.second];
  }

  vector<Point3D> planar_sites;
  for (int i = 0; i < points.size(); ++i) {
    Point3D point(points[i].index, points[i].x, points[i].y, 0);
    planar_sites.emplace_back(point);
  }

  vector<Point3D> border = GetContexHull(planar_sites);

  for (const Point3D &point : border) {
    is_on_border[point.index] = true;
  }

  for (int i = 0; i < points.size(); ++i) {
    if (!is_on_border[i]) {
      polygons_count += 1;
      edges_count += edge_degree[i];
    }
  }

  if (polygons_count == 0) {
    return 0;
  }

  return edges_count / polygons_count;
}


int main() {
  double x, y;
  int i = 0;
  vector<Point3D> points;

  while (std::cin >> x >> y) {
    Point3D point(i, x, y, x * x + y * y);
    rotate(point, angle_to_rotate);
    points.emplace_back(point);
    ++i;
  }

  std::cout << std::setprecision(10) <<
            GetAverageSidesInVoronoiPolygons(points);
}