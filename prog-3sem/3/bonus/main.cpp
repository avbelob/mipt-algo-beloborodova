#include <algorithm>
#include <iostream>
#include <vector>

using std::swap;
using std::sort;
using std::vector;
using std::find;

struct Point2D {
  double x;
  double y;

  explicit Point2D(const double &x, const double &y);

  bool operator<(const Point2D &second_point);

  bool operator==(const Point2D &second_point);
};

Point2D::Point2D(const double &x, const double &y) : x(x), y(y) {}

bool Point2D::operator<(const Point2D &second_point) {
  return (this->x < second_point.x) ||
         (this->x == second_point.x && this->y < second_point.y);
}

bool Point2D::operator==(const Point2D &second_point) {
  return (this->x == second_point.x) && (this->y == second_point.y);
}

bool IsOrientationRight(const Point2D &first_point, const Point2D &second_point,
                        const Point2D &third_point) {

  return first_point.x * (second_point.y - third_point.y) +
         second_point.x * (third_point.y - first_point.y) +
         third_point.x * (first_point.y - second_point.y) < 0;

}

bool IsOrientationLeft(const Point2D &first_point, const Point2D &second_point,
                       const Point2D &third_point) {

  return first_point.x * (second_point.y - third_point.y) +
         second_point.x * (third_point.y - first_point.y) +
         third_point.x * (first_point.y - second_point.y) > 0;

}

vector<Point2D> GetContexHull(vector<Point2D> &points) {
  vector<Point2D> contex_hull;
  if (points.size() <= 2) {
    return contex_hull;
  }
  sort(points.begin(), points.end());
  Point2D start = points[0];
  Point2D end = points.back();
  vector<Point2D> up, down;
  up.emplace_back(start);
  down.emplace_back(start);
  for (int i = 1; i < points.size(); ++i) {
    if (i == points.size() - 1 || IsOrientationRight(start, points[i], end)) {
      while (up.size() >= 2 &&
             !IsOrientationRight(up[up.size() - 2], up[up.size() - 1],
                                 points[i])) {
        up.pop_back();
      }
      up.emplace_back(points[i]);
    }
    if (i == points.size() - 1 || IsOrientationLeft(start, points[i], end)) {
      while (down.size() >= 2 &&
             !IsOrientationLeft(down[down.size() - 2], down[down.size() - 1],
                                points[i])) {
        down.pop_back();
      }
      down.emplace_back(points[i]);
    }
  }

  for (int i = 0; i < up.size(); ++i) {
    contex_hull.push_back(up[i]);
  }
  for (int i = down.size() - 2; i > 0; --i) {
    contex_hull.push_back(down[i]);
  }
  return contex_hull;
}

int GetMaxFencesCount(vector<Point2D>& points) {
  int max_fences_count = 0;

  vector<Point2D> convex_hull = GetContexHull(points);

  while (convex_hull.size() >= 3) {
    ++max_fences_count;
    vector<int> index_to_delete;
    for (int i = 0; i < points.size(); ++i) {
      for (int j = 0; j < convex_hull.size(); ++j) {
        if (points[i] == convex_hull[j]) {
          if (find(index_to_delete.begin(), index_to_delete.end(), i) ==
              index_to_delete.end()) {
            index_to_delete.emplace_back(i);
          }
        } else {
          if (!IsOrientationRight(convex_hull[j], points[i],
                                  convex_hull[(j + 1) % convex_hull.size()]) &&
              !IsOrientationLeft(convex_hull[j], points[i],
                                 convex_hull[(j + 1) % convex_hull.size()])) {
            if (find(index_to_delete.begin(), index_to_delete.end(), i) ==
                index_to_delete.end()) {
              index_to_delete.emplace_back(i);
            }
          }
        }
      }
    }
    sort(index_to_delete.begin(), index_to_delete.end());

    for (int i = index_to_delete.size() - 1; i >= 0; --i) {
      points.erase(points.begin() + index_to_delete[i]);
    }

    convex_hull.clear();
    convex_hull = GetContexHull(points);
  }
  return max_fences_count;
}

int main() {
  int points_count;
  std::cin >> points_count;
  vector<Point2D> points;
  for (int i = 0; i < points_count; ++i) {
    double x;
    double y;
    std::cin >> x >> y;
    Point2D point(x, y);
    points.emplace_back(point);
  }

  std::cout << GetMaxFencesCount(points);
}