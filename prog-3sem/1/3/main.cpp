#include <iostream>
#include <string>
#include <vector>
#include <map>

using std::vector;
using std::map;
using std::string;

enum { alphabet_size = 26, first_char = 'a' };

class Bohr {
public:
    explicit Bohr(int max_pattern_size);

    explicit Bohr(const Bohr* bohr) = delete;

    ~Bohr();

    void AddString(const string& str, const int& pattern_number);

    int GetSuffixLink(const int& cur_index);

    int GetTransitionByChar(const int cur_index, const char& cur_char);

    int GetCompressSuffixLink(const int& cur_index);

    bool IsTerminalNode(const int& cur_index);

    vector<int> GetTerminalPatternNumber(const int& cur_index);

    int GetLengthToRoot(const int& cur_index);

private:
    struct Vertex {
        int parent;
        char char_to_parent;
        map<char, int> children;
        vector<int> transitions;
        bool is_terminal;
        int suffix_link;
        int compress_suffix_link;
        vector<int> terminal_pattern_number;
        int length_to_root;

        Vertex();
    };

    vector<Vertex> vertices_;
    int size_;
};

Bohr::Vertex::Vertex() : parent(0),
                         is_terminal(false),
                         suffix_link(-1),
                         compress_suffix_link(-1),
                         transitions(alphabet_size, -1),
                         length_to_root(0) {}

Bohr::Bohr(int max_pattern_size) : vertices_(max_pattern_size), size_(1) {}

Bohr::~Bohr() = default;

void Bohr::AddString(const string& str, const int& pattern_number) {
  int cur_index = 0;
  map<char, int>* cur_children = &vertices_[cur_index].children;

  for (int i = 0; i < str.length(); ++i) {
    char curr_char = str[i];

    if (cur_children->find(curr_char) == cur_children->end()) {
      cur_children->insert({curr_char, size_});
      vertices_[size_].parent = cur_index;
      vertices_[size_].char_to_parent = curr_char;
      ++size_;
    }
    cur_index = cur_children->find(curr_char)->second;
    cur_children = &vertices_[cur_index].children;
  }

  vertices_[cur_index].length_to_root = (int)str.length();
  vertices_[cur_index].is_terminal = true;
  vertices_[cur_index].terminal_pattern_number.emplace_back(pattern_number);
}

int Bohr::GetSuffixLink(const int& cur_index) {
  if (vertices_[cur_index].suffix_link == -1) {
    if (cur_index == 0 || vertices_[cur_index].parent == 0) {
      vertices_[cur_index].suffix_link = 0;
    } else {
      vertices_[cur_index].suffix_link =
              GetTransitionByChar(GetSuffixLink(vertices_[cur_index].parent),
                                  vertices_[cur_index].char_to_parent);
    }
  }

  return vertices_[cur_index].suffix_link;
}

int Bohr::GetTransitionByChar(const int cur_index, const char& cur_char) {
  if (vertices_[cur_index].transitions[cur_char - first_char] == -1) {
    if (vertices_[cur_index].children.find(cur_char) !=
        vertices_[cur_index].children.end()) {
      vertices_[cur_index].transitions[cur_char - first_char] =
              vertices_[cur_index].children.find(cur_char)->second;
    } else {
      if (cur_index == 0) {
        vertices_[cur_index].transitions[cur_char - first_char] = 0;
      } else {
        vertices_[cur_index].transitions[cur_char - first_char] =
                GetTransitionByChar(GetSuffixLink(cur_index), cur_char);
      }
    }
  }

  return vertices_[cur_index].transitions[cur_char - first_char];
}

int Bohr::GetCompressSuffixLink(const int& cur_index) {
  if (vertices_[cur_index].compress_suffix_link == -1) {
    int cur_suffix_link = GetSuffixLink(cur_index);
    if (vertices_[cur_suffix_link].is_terminal || cur_suffix_link == 0) {
      vertices_[cur_index].compress_suffix_link = cur_suffix_link;
    } else {
      vertices_[cur_index].compress_suffix_link =
              GetCompressSuffixLink(cur_suffix_link);
    }
  }

  return vertices_[cur_index].compress_suffix_link;
}

bool Bohr::IsTerminalNode(const int& cur_index) {
  return vertices_[cur_index].is_terminal;
}

vector<int> Bohr::GetTerminalPatternNumber(const int& cur_index) {
  return vertices_[cur_index].terminal_pattern_number;
}

int Bohr::GetLengthToRoot(const int& cur_index) {
  return vertices_[cur_index].length_to_root;
}

void CalcSubstringsCount(Bohr& bohr, const int cur_index,
                         vector<int>& substrings_count,
                         const vector<int>& starting_positions,
                         const int& str_without_last_questions_count,
                         const int& index_in_str) {
  vector<int> cur_terminal_pattern_num =
          bohr.GetTerminalPatternNumber(cur_index);
  int curr_positions_index = 0;
  for (int j = 0; j < cur_terminal_pattern_num.size(); ++j) {
    curr_positions_index = index_in_str - bohr.GetLengthToRoot(cur_index) + 1;
    if (curr_positions_index >=
        starting_positions[cur_terminal_pattern_num[j]] &&
        curr_positions_index < str_without_last_questions_count) {
      ++substrings_count[curr_positions_index -
                         starting_positions[cur_terminal_pattern_num[j]]];
    }
  }
}

vector<int> GetSubstringsCount(Bohr& bohr, const string& main_str,
                               const vector<int>& starting_positions,
                               const int& str_without_last_questions_count) {
  vector<int> substrings_count(main_str.length(), 0);
  char cur_char;
  int cur_index = 0;
  vector<int> cur_terminal_pattern_number;

  for (int i = 0; i < main_str.length(); ++i) {
    cur_char = main_str[i];
    cur_index = bohr.GetTransitionByChar(cur_index, cur_char);

    if (bohr.IsTerminalNode(cur_index)) {
      CalcSubstringsCount(bohr, cur_index, substrings_count, starting_positions,
                          str_without_last_questions_count, i);
    }

    int cur_comprex_suff_index = cur_index;

    while (bohr.GetCompressSuffixLink(cur_comprex_suff_index) != 0) {
      cur_comprex_suff_index =
              bohr.GetCompressSuffixLink(cur_comprex_suff_index);
      CalcSubstringsCount(bohr, cur_comprex_suff_index, substrings_count,
                          starting_positions, str_without_last_questions_count,
                          i);
    }
  }

  return substrings_count;
}

int main() {
  string patterns;
  std::cin >> patterns;
  int questions_at_the_end_count = 1;

  vector<int> starting_positions;
  int starting_position = 0;
  Bohr bohr((int)patterns.length());
  string pattern;
  int pattern_number = 0;
  while (patterns.find('?') != string::npos) {
    pattern = patterns.substr(0, patterns.find('?'));
    if (!pattern.empty()) {
      questions_at_the_end_count = 1;
      bohr.AddString(pattern, pattern_number);
      starting_positions.emplace_back(starting_position);
      ++pattern_number;
    }
    starting_position += patterns.find('?') + 1;
    if (patterns.find('?') == 0) {
      ++questions_at_the_end_count;
    }

    patterns.erase(patterns.begin(), patterns.begin() + patterns.find('?') + 1);
  }
  if (!patterns.empty()) {
    questions_at_the_end_count = 0;
    bohr.AddString(patterns, pattern_number);
    starting_positions.emplace_back(starting_position);
    ++pattern_number;
  }

  string main_string;
  std::cin >> main_string;

  int str_without_last_questions_count =
          (int)main_string.length() - questions_at_the_end_count;

  vector<int> substrings_count = GetSubstringsCount(
          bohr, main_string, starting_positions, str_without_last_questions_count);

  for (int i = 0; i < substrings_count.size(); ++i) {
    if (substrings_count[i] == pattern_number) {
      std::cout << i << " ";
    }
  }
}