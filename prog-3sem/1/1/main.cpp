#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::min;
using std::max;
using std::cin;

string GetMainString(int pattern_length) {
  string main_string;
  char current_char;
  int current_index = 0;

  while (current_index < pattern_length && cin >> current_char) {
    main_string += current_char;
    ++current_index;
  }

  return main_string;
}

bool IsNewCharExist(string &glued_str, int pattern_length, int shape) {
  char current_char;
  if (cin >> current_char) {
    glued_str[pattern_length + 1 + shape] = current_char;
    return true;
  }
  return false;
}

vector<int> GetZFunction(const string& str) {
  int str_length = str.length();
  vector<int> z_function(str_length, 0);

  int z_left_index = 0;
  int z_right_index = 0;

  for (int i = 1; i < str_length; ++i) {
    z_function[i] = max(min(z_right_index - i + 1,
                            z_function[i - z_left_index]), 0);

    while (i + z_function[i] < str_length && str[z_function[i]] ==
                                             str[i + z_function[i]]) {
      ++z_function[i];
    }

    if (i + z_function[i] - z_left_index > z_right_index) {
      z_left_index = i;
      z_right_index = i + z_function[i] - 1;
    }
  }

  return z_function;
}

vector<int> GetEntryPositionsIndex(const string& pattern) {
  vector<int> entry_positions_index;
  string glued_str = pattern + "#" + GetMainString(pattern.length());
  vector<int> z_function = GetZFunction(pattern);

  int z_left_coord = 0;
  int z_right_coord = 0;
  int curr_index = pattern.length() + 1;
  int shape = 0;

  for (int i = pattern.length() + 1;; ++i) {
    int curr_z_value;
    curr_z_value = max(min(z_right_coord - i, z_function[i - z_left_coord]), 0);

    while (curr_index + curr_z_value - shape < glued_str.length() &&
           glued_str[curr_z_value] ==
           glued_str[curr_index + (shape + curr_z_value) % pattern.length()]) {
      ++curr_z_value;
    }

    if (i + curr_z_value >= z_right_coord) {
      z_left_coord = i;
      z_right_coord = i + curr_z_value;
    }

    if (curr_z_value == pattern.length()) {
      entry_positions_index.emplace_back(i - pattern.length() - 1);
    }

    if (!IsNewCharExist(glued_str, pattern.length(), shape)) {
      break;
    }

    shape = (shape + 1) % pattern.length();
  }

  return entry_positions_index;
}

int main() {
  string pattern;
  cin >> pattern;

  vector<int> entry_positions_index(GetEntryPositionsIndex(pattern));

  for(int i = 0; i < entry_positions_index.size(); ++i) {
    std::cout << entry_positions_index[i] << " ";
  }
}