#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <assert.h>

using std::string;
using std::vector;
using std::min;
using std::max;

// z-функция по строке
vector<int> GetZFunctionByString(const string& str) {
  int str_length = str.length();
  vector<int> z_function(str_length, 0);

  int z_left_index = 0;
  int z_right_index = 0;

  for (int i = 1; i < str_length; ++i) {
    z_function[i] = max(min(z_right_index - i + 1,
                            z_function[i - z_left_index]), 0);

    while (i + z_function[i] < str_length && str[z_function[i]] ==
                                             str[i + z_function[i]]) {
      ++z_function[i];
    }

    if (i + z_function[i] - z_left_index > z_right_index) {
      z_left_index = i;
      z_right_index = i + z_function[i] - 1;
    }
  }

  return z_function;
}

// префикс-функция по строке
vector<int> GetPrefixFunctionByString(const string& str) {
  int str_length = str.length();
  vector<int> prefix_function(str_length);
  prefix_function[0] = 0;
  for (int i = 1; i < str_length; ++i) {
    int current_value = prefix_function[i - 1];

    while (current_value > 0 && str[i] != str[current_value]) {
      current_value = prefix_function[(current_value - 1)];

    }

    if (str[i] == str[current_value]) {
      ++current_value;
    }

    prefix_function[i] = current_value;

  }
  return prefix_function;
}

// строка по префикс-функции
string GetStringByPrefixFunction(const vector<int>& prefix_function) {
  if (prefix_function.size() == 0) {
    return "";
  }
  string str_by_prefix_function = "a";

  for (int i = 1; i < prefix_function.size(); ++i) {

    if (prefix_function[i] != 0) {
      str_by_prefix_function += str_by_prefix_function[prefix_function[i] - 1];

    } else {
      std::set<char> forbidden_char;
      int current_value = prefix_function[i - 1];

      while (current_value > 0) {
        forbidden_char.emplace(str_by_prefix_function[current_value]);
        current_value = prefix_function[current_value - 1];
      }

      for (char i = 'b'; i <= 'z'; ++i) {
        if (forbidden_char.find(i) == forbidden_char.end()) {
          str_by_prefix_function += i;
          break;
        }
      }
    }
  }
  return str_by_prefix_function;
}

// строка по z-функции
string GetStringByZFunction(const vector<int>& z_function) {
  if (z_function.size() == 0) {
    return "";
  }
  string str_by_z_function = "a";
  int prefix_length = 0;
  int z_left_index = 0;
  int z_right_index = 0;
  int char_index = 0;
  std::set<char> forbidden_char;

  for (int i = 1; i < z_function.size(); ++i) {
    if (z_function[i] == 0 && prefix_length == 0) {
      if (z_right_index == i - 1) {
        forbidden_char.emplace(str_by_z_function[z_function[z_left_index]]);
      }
      for (char i = 'b'; i <= 'z'; ++i) {
        if (forbidden_char.find(i) == forbidden_char.end()) {
          str_by_z_function += i;
          break;
        }
      }
      forbidden_char.clear();
    }

    if (z_function[i] > prefix_length) {
      prefix_length = z_function[i];
      z_left_index = i;
      z_right_index = i + z_function[i] - 1;
      char_index = 0;
    } else if (z_function[i] == prefix_length) {
      forbidden_char.emplace(str_by_z_function[z_function[i]]);
    }

    if (prefix_length > 0) {
      str_by_z_function += str_by_z_function[char_index];
      ++char_index;
      --prefix_length;
    }
  }
  return str_by_z_function;
}

// префикс-функция по z-функции
vector<int> GetPrefixFunctionByZFunction(vector<int>& z_function) {
  z_function[0] = 0;
  vector<int> prefix_function(z_function.size(), 0);
  for (int i = 1; i < z_function.size(); ++i) {
    for (int j = z_function[i] - 1; j >= 0; --j) {
      if (prefix_function[i + j] > 0) {
        break;
      } else {
        prefix_function[i + j] = j + 1;
      }
    }
  }

  return prefix_function;
}

// z-функция по префикс-функции
vector<int> GetZFunctionByPrefixFunction(vector<int>& prefix_function) {
  prefix_function[0] = 0;
  vector<int> z_function(prefix_function.size(), 0);

  for (int i = 1; i < prefix_function.size(); ++i) {
    if (prefix_function[i] > 0) {
      z_function[i - prefix_function[i] + 1] = prefix_function[i];
    }
  }
  int cur_index = 1;

  while (cur_index < prefix_function.size()) {
    int next_index = cur_index;
    if (z_function[cur_index] > 0) {

      for (int i = 1; i < z_function[cur_index]; ++i) {
        if (z_function[cur_index + i] > z_function[i]) {
          break;
        }

        z_function[cur_index + i] = min(z_function[i], z_function[cur_index]
        - i);
        next_index = cur_index + i;
      }
    }
    cur_index = next_index + 1;
  }

  return z_function;
}

void TestAll() {
  string str = "abbacbbbbaabcaaba";
  vector<int> prefix_function =
          {0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 2, 0, 1, 1, 2, 1};
  vector<int> z_function = {0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 2, 0, 0, 1, 2, 0, 1};

  assert(GetPrefixFunctionByString(str) == prefix_function);
  assert(GetZFunctionByString(str) == z_function);

  assert(GetStringByPrefixFunction(prefix_function) == str);
  assert(GetStringByZFunction(z_function) == str);

  assert(GetPrefixFunctionByZFunction(z_function) == prefix_function);
  assert(GetZFunctionByPrefixFunction(prefix_function) == z_function);
}

int main() {
  TestAll();
}