#include <iostream>
#include <string>
#include <vector>
#include <map>

using std::vector;
using std::map;
using std::string;

enum {
    alphabet_size = 2,
    first_char = '0',
    max_len_of_all_words = 30000
};

class Bohr {
public:
    explicit Bohr(const int max_pattern_size);

    explicit Bohr(const Bohr *bohr) = delete;

    ~Bohr();

    void AddString(const string &str, const int &pattern_number);

    int GetSuffixLink(const int &cur_index);

    int GetTransitionByChar(const int cur_index, const char &cur_char);

    int GetCompressSuffixLink(const int &cur_index);

    bool IsTerminalNode(const int &cur_index);

    void FindCycle(const int cur_index, vector<string> &color, vector<bool>
            &is_it_banned);

    bool IsThereCycle();

private:
    struct Vertex {
        int parent;
        char char_to_parent;
        map<char, int> children;
        vector<int> transitions;
        bool is_terminal;
        int suffix_link;
        int compress_suffix_link;

        Vertex();
    };

    vector<Vertex> vertices_;

    int size_;
    bool is_there_cycle_;
};

Bohr::Vertex::Vertex() : parent(0),
                         is_terminal(false),
                         suffix_link(-1),
                         compress_suffix_link(-1),
                         transitions(alphabet_size, -1) {}

Bohr::Bohr(int max_pattern_size) : vertices_(max_pattern_size), size_(1),
                                   is_there_cycle_(false) {}

Bohr::~Bohr() = default;

void Bohr::AddString(const string &str, const int &pattern_number) {
  int cur_index = 0;
  map<char, int> *cur_children = &vertices_[cur_index].children;

  for (int i = 0; i < str.length(); ++i) {
    char curr_char = str[i];

    if (cur_children->find(curr_char) == cur_children->end()) {
      cur_children->insert({curr_char, size_});
      vertices_[size_].parent = cur_index;
      vertices_[size_].char_to_parent = curr_char;
      ++size_;
    }
    cur_index = cur_children->find(curr_char)->second;
    cur_children = &vertices_[cur_index].children;
  }

  vertices_[cur_index].is_terminal = true;
}

int Bohr::GetSuffixLink(const int &cur_index) {
  if (vertices_[cur_index].suffix_link == -1) {
    if (cur_index == 0 || vertices_[cur_index].parent == 0) {
      vertices_[cur_index].suffix_link = 0;
    } else {
      vertices_[cur_index].suffix_link =
              GetTransitionByChar(GetSuffixLink(vertices_[cur_index].parent),
                                  vertices_[cur_index].char_to_parent);
    }
  }

  return vertices_[cur_index].suffix_link;
}

int Bohr::GetTransitionByChar(const int cur_index, const char &cur_char) {
  if (vertices_[cur_index].transitions[cur_char - first_char] == -1) {
    if (vertices_[cur_index].children.find(cur_char) !=
        vertices_[cur_index].children.end()) {
      vertices_[cur_index].transitions[cur_char - first_char] =
              vertices_[cur_index].children.find(cur_char)->second;
    } else {
      if (cur_index == 0) {
        vertices_[cur_index].transitions[cur_char - first_char] = 0;
      } else {
        vertices_[cur_index].transitions[cur_char - first_char] =
                GetTransitionByChar(GetSuffixLink(cur_index), cur_char);
      }
    }
  }

  return vertices_[cur_index].transitions[cur_char - first_char];
}

int Bohr::GetCompressSuffixLink(const int &cur_index) {
  if (vertices_[cur_index].compress_suffix_link == -1) {
    int cur_suffix_link = GetSuffixLink(cur_index);
    if (vertices_[cur_suffix_link].is_terminal || cur_suffix_link == 0) {
      vertices_[cur_index].compress_suffix_link = cur_suffix_link;
    } else {
      vertices_[cur_index].compress_suffix_link =
              GetCompressSuffixLink(cur_suffix_link);
    }
  }

  return vertices_[cur_index].compress_suffix_link;
}

bool Bohr::IsTerminalNode(const int &cur_index) {
  return vertices_[cur_index].is_terminal;
}

void Bohr::FindCycle(const int cur_index, vector<string> &color, vector<bool>
        &is_it_banned) {
  color[cur_index] = "gray";
  char cur_char;
  for (char i = 0; i < alphabet_size; ++i) {
    cur_char = first_char + i;
    int next_vertices = GetTransitionByChar(cur_index, cur_char);
    if (IsTerminalNode(next_vertices) ||
        GetCompressSuffixLink(next_vertices) != 0) {
      is_it_banned[next_vertices] = true;
    }
    if (!is_it_banned[next_vertices]) {
      if (color[next_vertices] == "white") {
        FindCycle(next_vertices, color, is_it_banned);
      }

      if (color[next_vertices] == "gray") {
        is_there_cycle_ = true;
      }
    }
  }
  if (is_it_banned[GetTransitionByChar(cur_index, cur_char ='0')] &&
      is_it_banned[GetTransitionByChar(cur_index, cur_char ='1')]) {
    is_it_banned[cur_index] = true;
  }
  color[cur_index] = "black";
}

bool Bohr::IsThereCycle() {
  vector<string> color(size_, "white");
  vector<bool> is_it_banned(size_, false);
  FindCycle(0, color, is_it_banned);
  return is_there_cycle_;
}

int main() {
  int all_virus_codes_count = 0;
  std::cin >> all_virus_codes_count;
  string pattern;
  Bohr bohr(max_len_of_all_words);

  for (int i = 0; i < all_virus_codes_count; ++i) {
    std::cin >> pattern;
    bohr.AddString(pattern, i);
  }

  if (bohr.IsThereCycle()) {
    std::cout << "TAK";
  } else {
    std::cout << "NIE";
  }
}