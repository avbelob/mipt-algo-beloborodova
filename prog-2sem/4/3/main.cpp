/*
Условие:
Задано дерево с корнем, содержащее (1 ≤ n ≤ 100 000) вершин,
 пронумерованных от 0 до n-1.
Требуется ответить на m (1 ≤ m ≤ 10 000 000) запросов о
 наименьшем общем предке для пары вершин.
Запросы генерируются следующим образом.
 Заданы числа a1, a2 и числа x, y и z.
Числа a3, ..., a2m генерируются следующим образом:
 ai = (x ⋅ ai-2 + y ⋅ ai-1 + z) mod n.
Первый запрос имеет вид (a1, a2).
 Если ответ на i-1-й запрос равен v,
 то i-й запрос имеет вид ((a2i-1 + v) mod n, a2i).

Для решения задачи можно использовать метод двоичного подъёма.

Формат ввода
Первая строка содержит два числа: n и m.
Корень дерева имеет номер 0.
Вторая строка содержит n-1 целых чисел,
 i-е из этих чисел равно номеру родителя вершины i.
Третья строка содержит два целых числа в диапазоне от 0 до n-1: a1 и a2.
Четвертая строка содержит три целых числа: x, y и z,
 эти числа неотрицательны и не превосходят 109.

Формат вывода
Выведите в выходной файл сумму номеров вершин — ответов на все запросы.
 */

#include <cmath>
#include <iostream>
#include <vector>

using std::pair;
using std::vector;

class NewQueryGenerator {
 public:
  explicit NewQueryGenerator(long long int a, long long int b, long long int x,
                             long long int y, long long int z,
                             long long int numbers_amount);

  pair<long long int, long long int> Generate(long long int current_result);

 private:
  long long int numbers_amount_;
  long long int a_;
  long long int b_;
  long long int x_;
  long long int y_;
  long long int z_;
};

NewQueryGenerator::NewQueryGenerator(long long int a, long long int b,
                                     long long int x, long long int y,
                                     long long int z,
                                     long long int numbers_amount)
    : a_(a), b_(b), x_(x), y_(y), z_(z), numbers_amount_(numbers_amount) {}

pair<long long int, long long int> NewQueryGenerator::Generate(
    long long int current_result) {
  pair<long long int, long long int> new_pair(
      (a_ + current_result) % numbers_amount_, b_);
  a_ = (a_ * x_ + b_ * y_ + z_) % numbers_amount_;
  b_ = (b_ * x_ + a_ * y_ + z_) % numbers_amount_;
  return new_pair;
}

class Tree {
 public:
  Tree(vector<vector<long long int>> &parents)
      : tree_(parents),
        parents_table_size_(ceil(log2(parents.size()))),
        time_in_dfs_in_(parents.size()),
        time_in_dfs_out_(parents.size()),
        ancestor_sparse_table_(parents.size(),
                               vector<long long int>(parents_table_size_ + 1)),
        dfs_time_(0) {
    SparseTableCalc(0, 0);
  }

  long long int GetLCA(pair<long long int, long long int> request);

 private:
  void SparseTableCalc(long long int node, long long int parent);

  bool IsFirstAncestorOfSecond(long long int first, long long int second);

  long long int parents_table_size_;

  long long int dfs_time_;
  vector<long long int> time_in_dfs_in_;
  vector<long long int> time_in_dfs_out_;

  vector<vector<long long int>> tree_;
  vector<vector<long long int>> ancestor_sparse_table_;
};

void Tree::SparseTableCalc(long long int node, long long int parent) {
  time_in_dfs_in_[node] = dfs_time_;
  ++dfs_time_;
  ancestor_sparse_table_[node][0] = parent;
  for (int i = 1; i <= parents_table_size_; ++i) {
    ancestor_sparse_table_[node][i] =
        ancestor_sparse_table_[ancestor_sparse_table_[node][i - 1]][i - 1];
  }

  for (int i = 0; i < tree_[node].size(); ++i) {
    long long int node_child = tree_[node][i];
    if (node_child != parent) {
      SparseTableCalc(node_child, node);
    }
  }

  time_in_dfs_out_[node] = dfs_time_;
  ++dfs_time_;
}

bool Tree::IsFirstAncestorOfSecond(long long int first, long long int second) {
  return (time_in_dfs_in_[first] <= time_in_dfs_in_[second]) &&
         (time_in_dfs_out_[second] <= time_in_dfs_out_[first]);
}

long long int Tree::GetLCA(pair<long long int, long long int> request) {
  if (IsFirstAncestorOfSecond(request.first, request.second)) {
    return request.first;
  }
  if (IsFirstAncestorOfSecond(request.second, request.first)) {
    return request.second;
  }
  for (long long int i = parents_table_size_; i >= 0; --i) {
    if (!IsFirstAncestorOfSecond(ancestor_sparse_table_[request.first][i],
                                 request.second)) {
      request.first = ancestor_sparse_table_[request.first][i];
    }
  }

  return ancestor_sparse_table_[request.first][0];
}

int main() {
  long long int numbers_amount = 0;
  long long int requests_amount = 0;
  std::cin >> numbers_amount >> requests_amount;
  vector<vector<long long int>> parents(numbers_amount);

  for (int i = 1; i < numbers_amount; ++i) {
    long long int parent;
    std::cin >> parent;
    parents[parent].push_back(i);
  }
  Tree tree(parents);

  long long int a_1;
  long long int b_1;
  long long int x;
  long long int y;
  long long int z;
  std::cin >> a_1 >> b_1 >> x >> y >> z;

  NewQueryGenerator new_query(a_1, b_1, x, y, z, numbers_amount);

  long long int current_result = 0;
  long long int vertex_numbers_sum = 0;

  for (int i = 0; i < requests_amount; ++i) {
    current_result = tree.GetLCA(new_query.Generate(current_result));
    vertex_numbers_sum += current_result;
  }
  std::cout << vertex_numbers_sum;
}