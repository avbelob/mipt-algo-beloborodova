/*
Условие:
Реализуйте структуру данных “массив строк”
на основе декартового дерева по неявному ключу со следующими методами:
// Добавление строки в позицию position.
// Все последующие строки сдвигаются на одну позицию вперед.
void InsertAt( int position, const std::string& value );
// Удаление строки из позиции position.
// Все последующие строки сдвигаются на одну позицию назад.
void DeleteAt( int position );
// Получение строки из позиции position.
std::string GetAt( int position );
Все методы должны работать за O(log n) в среднем,
где n – текущее количество строк в массиве.

Формат входных данных.
Первая строка содержит количество команд k ≤ 106.
Последующие k строк содержат описания команд:
Команда "+ 10 hello" означает добавление строки hello в позицию 10.
Команда “- 14 16” означает удаление строк от позиции 14
до позиции 16 включительно.
Команда “? 33” означает запрос на вывод строки из массива в позиции 33.

Формат выходных данных.
Выведите все строки, запрошенные командами “?”.
 */

#include <iostream>

using std::make_pair;
using std::pair;
using std::string;

struct Node {
  string value;
  int subtree_size;
  int priority;

  Node *left_child;
  Node *right_child;

  Node(const std::string &new_value) {
    value = new_value;
    priority = rand();
    left_child = nullptr;
    right_child = nullptr;
    subtree_size = 1;
  }

  void FixSubtreeSize() {
    subtree_size = 1;
    if (left_child != nullptr) {
      subtree_size += left_child->subtree_size;
    }
    if (right_child != nullptr) {
      subtree_size += right_child->subtree_size;
    }
  }
};

class CartesianTreeByImplicitKey {
 public:
  explicit CartesianTreeByImplicitKey();

  ~CartesianTreeByImplicitKey();

  void InsertAt(int position, const string &value);

  void DeleteAt(int position);

  string GetAt(int position);

 private:
  Node *root_;

  void Clear(Node *node);

  pair<Node *, Node *> Split(Node *node, int position);

  Node *Merge(Node *first_root, Node *second_root);
};

CartesianTreeByImplicitKey::CartesianTreeByImplicitKey() { root_ = nullptr; }

CartesianTreeByImplicitKey::~CartesianTreeByImplicitKey() { Clear(root_); }

void CartesianTreeByImplicitKey::InsertAt(int position, const string &value) {
  Node *new_node = new Node(value);
  pair<Node *, Node *> split_result = Split(root_, position);
  Node *right = Merge(new_node, split_result.second);
  root_ = Merge(split_result.first, right);
}

void CartesianTreeByImplicitKey::DeleteAt(int position) {
  pair<Node *, Node *> split_result = Split(root_, position);
  pair<Node *, Node *> second_split_result = Split(split_result.second, 1);
  delete second_split_result.first;
  root_ = Merge(split_result.first, second_split_result.second);
}

string CartesianTreeByImplicitKey::GetAt(int position) {
  Node *current_node = root_;
  while (true) {
    int current_position = 0;
    if (current_node->left_child != nullptr) {
      current_position = current_node->left_child->subtree_size;
    }
    if (current_position == position) {
      break;
    }

    if (position < current_position) {
      current_node = current_node->left_child;
    } else {
      position -= current_position + 1;
      current_node = current_node->right_child;
    }
  }
  return current_node->value;
}

void CartesianTreeByImplicitKey::Clear(Node *node) {
  if (node == nullptr) {
    return;
  }

  Clear(node->left_child);
  Clear(node->right_child);
  delete node;
}

pair<Node *, Node *> CartesianTreeByImplicitKey::Split(Node *node,
                                                       int position) {
  if (node == nullptr) {
    return pair<Node *, Node *>(nullptr, nullptr);
  }
  int current_position = 0;
  if (node->left_child != nullptr) {
    current_position = node->left_child->subtree_size;
  }

  if (current_position >= position) {
    std::pair<Node *, Node *> split_result = Split(node->left_child, position);
    node->left_child = split_result.second;
    node->FixSubtreeSize();
    return std::make_pair(split_result.first, node);
  }

  std::pair<Node *, Node *> split_result =
      Split(node->right_child, position - current_position - 1);
  node->right_child = split_result.first;
  node->FixSubtreeSize();
  return std::make_pair(node, split_result.second);
}

Node *CartesianTreeByImplicitKey::Merge(Node *first_root, Node *second_root) {
  if (second_root == nullptr) {
    return first_root;
  }

  if (first_root == nullptr) {
    return second_root;
  }

  if (first_root->priority > second_root->priority) {
    first_root->right_child = Merge(first_root->right_child, second_root);
    first_root->FixSubtreeSize();
    return first_root;
  }

  second_root->left_child = Merge(first_root, second_root->left_child);
  second_root->FixSubtreeSize();
  return second_root;
}

int main() {
  int requests_amount;
  std::cin >> requests_amount;
  CartesianTreeByImplicitKey string_array;

  for (int i = 0; i < requests_amount; ++i) {
    char command;
    int position = 0;
    std::cin >> command;
    std::cin >> position;

    if (command == '+') {
      string value;
      std::cin >> value;
      string_array.InsertAt(position, value);
    }

    if (command == '?') {
      std::cout << string_array.GetAt(position) << "\n";
    }

    if (command == '-') {
      int end_position = 0;
      std::cin >> end_position;
      for (int cur_pos = end_position; cur_pos >= position; --cur_pos) {
        string_array.DeleteAt(cur_pos);
      }
    }
  }
}