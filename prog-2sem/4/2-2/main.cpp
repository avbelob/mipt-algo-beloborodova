/*
2_1. Сумма на подотрезке.
Дан массив из целых чисел a1, a2, ..., an (индексация с 1!).
Для каждого запроса [left, right] найдите такой подотрезок al, al+1, ..., ar
этого массива (1 <= left <= l <= r  <= right <= n),
что сумма чисел al + al+1 + ... + ar является максимально возможной.
Требуемое время ответа на запрос - O(log n).

Формат входных данных.
Входные данные содержат один или несколько тестовых примеров.
Описание каждого из них начинается с двух чисел n и m -
длины массива и числа интересующих подотрезков.
В следующей строке содержится n чисел — элементы массива.
Каждое из этих чисел по абсо­лютной величине не превосходит 104.
Далее следуют описания подотрезков,
каждое описание состоит из двух чисел left и right ,
обозначаю­щих левый и правый конец подотрезка (1 <= left <= right  <= n).
Суммарная длина всех массивов,
а также суммарное число подотрезков не превосходит 105.

Формат выходных данных.
Для каждого из тестовых примеров выведите m чисел:
искомую максимальную сумму для каж­дого из подотрезков.

 */
#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

using std::cin;
using std::max;
using std::pair;
using std::vector;

int MaxOfThree(int first, int second, int third) {
  if (first >= second && first >= third) {
    return first;
  }
  return max(second, third);
}

struct TreeTop {
  int units_max_sum;
  int units_max_prefix_sum;
  int units_max_suffix_sum;

  TreeTop(int sum = 0, int max_sum = 0, int max_prefix_sum = 0,
          int max_suffix_sum = 0) :
          units_max_sum(max_sum),
          units_max_prefix_sum(max_prefix_sum),
          units_max_suffix_sum(max_suffix_sum) {}
};

class SegmentTree {
 public:
  SegmentTree(vector<int> &sequence);

  int GetMaxSum(int left, int right);

  TreeTop GetParent(TreeTop left_child, TreeTop right_child);

 private:
  int size_;
  std::vector<TreeTop> segment_tree_;
};

TreeTop SegmentTree::GetParent(TreeTop left_child, TreeTop right_child) {

  int max_sum = MaxOfThree(left_child.units_max_sum,
                           right_child.units_max_sum,
                           left_child.units_max_suffix_sum +
                           right_child.units_max_prefix_sum);

  int max_prefix_sum = left_child.units_max_prefix_sum;
  if (left_child.units_max_prefix_sum == left_child.units_max_sum) {
    max_prefix_sum += right_child.units_max_prefix_sum;
  }

  int max_suffix_sum = right_child.units_max_suffix_sum;
  if (right_child.units_max_suffix_sum == right_child.units_max_sum) {
    max_suffix_sum += left_child.units_max_suffix_sum;
  }

  return TreeTop(max_sum, max_prefix_sum, max_suffix_sum);
}

SegmentTree::SegmentTree(vector<int> &sequence) {
  size_ = sequence.size();
  segment_tree_.resize(2 * size_ - 1);
  for (int i = 0; i < size_; ++i) {
    segment_tree_[size_ + i - 1] = TreeTop(sequence[i], sequence[i],
            sequence[i]);
  }
  for (int i = size_ - 2; i >= 0; --i) {
    segment_tree_[i] = GetParent(segment_tree_[2 * i + 1],
                                 segment_tree_[2 *i + 2]);
  }
}

int SegmentTree::GetMaxSum(int left, int right) {
  TreeTop left_res = TreeTop (0, 0, 0);

  TreeTop right_res = TreeTop (0, 0, 0);

  while (left < right) {

    if (left % 2 == 0) {
      left_res = GetParent(left_res, segment_tree_[left]);
    }

    left = left / 2;

    if (right % 2 == 1) {
      right_res = GetParent(segment_tree_[right], right_res);
    }

    right = right / 2 - 1;
  }

  if (left == right) {
    left_res = GetParent(left_res, segment_tree_[left]);
  }

  return GetParent(left_res, right_res).units_max_sum;
}

int main() {
  int numbers_amount = 0;
  int requests_amount = 0;
  while (cin >> numbers_amount >> requests_amount) {

    int size = 1 << (int) ceil(log2(numbers_amount));
    std::vector<int> sequence(size, 0);
    for (int i = 0; i < numbers_amount; ++i) {
      std::cin >> sequence[i];
    }

    SegmentTree tree(sequence);

    for (int i = 0; i < requests_amount; ++i) {
      int left = 0;
      int right = 0;
      std::cin >> left >> right;
      std::cout <<
                tree.GetMaxSum(left + size - 2, right + size - 2) << "\n";
    }
  }
}