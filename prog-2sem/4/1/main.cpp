/*
Условие:
Дано число N и последовательность из N целых чисел.
Найти вторую порядковую статистику на заданных диапазонах.
Для решения задачи используйте структуру данных Sparse Table.
Требуемое время обработки каждого диапазона O(1).
Время подготовки структуры данных O(n log n).

Формат входных данных.
В первой строке заданы 2 числа:
размер последовательности N и количество диапазонов M.
Следующие N целых чисел задают последовательность.
Далее вводятся M пар чисел - границ диапазонов.
Гарантируется,
что каждый диапазон содержит как минимум 2 элемента.

Формат выходных данных.
Для каждого из M диапазонов напечатать
элемент последовательности - 2ю порядковую статистику.
По одному числу в строке.

 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

using std::max;
using std::min;
using std::pair;
using std::vector;

class SparseTable {
 public:
  explicit SparseTable(vector<int> &sequence);

  pair<int, int> FindMinPair(pair<int, int> first_pair,
                             pair<int, int> second_pair);

  int FindSecondStatistics(int from, int to);

 private:
  vector<vector<pair<int, int>>> sparse_table_;
};

SparseTable::SparseTable(std::vector<int> &sequence) {
  int size = sequence.size();
  sparse_table_.assign(ceil(log2(size)),
                       vector<pair<int, int>>(size, pair<int, int>(0, 0)));

  for (int i = 0; i < size; ++i) {
    sparse_table_[0][i].first = sequence[i];
    sparse_table_[0][i].second = INT32_MAX;
  }

  for (int i = 1; i < sparse_table_.size(); ++i) {
    for (int j = 0; j < (size - pow(2, i) + 1); ++j) {
      pair<int, int> cur_pair = FindMinPair(
          sparse_table_[i - 1][j], sparse_table_[i - 1][j + pow(2, i - 1)]);
      sparse_table_[i][j].first = cur_pair.first;
      sparse_table_[i][j].second = cur_pair.second;
    }
  }
}

pair<int, int> SparseTable::FindMinPair(pair<int, int> first_pair,
                                        pair<int, int> second_pair) {
  pair<int, int> min_pair;
  vector<int> items;
  items.push_back(first_pair.first);
  items.push_back(second_pair.first);
  items.push_back(first_pair.second);
  items.push_back(second_pair.second);

  std::sort(items.begin(), items.end());

  min_pair.first = items[0];

  for (int i = 1; i < items.size(); ++i) {
    if (items[i] != items[0] || i == items.size() - 1) {
      min_pair.second = items[i];
      break;
    }
  }

  return min_pair;
}

int SparseTable::FindSecondStatistics(int from, int to) {
  int log_2 = ceil(log2(to - from + 1)) - 1;

  pair<int, int> min_pair = FindMinPair(
      sparse_table_[log_2][from], sparse_table_[log_2][to - pow(2, log_2) + 1]);

  return min_pair.second;
}

int main() {
  int numbers_amount = 0;
  int requests_amount = 0;
  std::cin >> numbers_amount >> requests_amount;

  vector<int> sequence(numbers_amount);

  for (int i = 0; i < numbers_amount; ++i) {
    std::cin >> sequence[i];
  }

  SparseTable sparse_table(sequence);

  vector<pair<int, int>> request(requests_amount);

  for (int i = 0; i < requests_amount; ++i) {
    std::cin >> request[i].first >> request[i].second;
    --request[i].first;
    --request[i].second;
  }

  for (int i = 0; i < requests_amount; ++i) {
    std::cout << sparse_table.FindSecondStatistics(request[i].first,
                                                   request[i].second)
              << "\n";
  }
}