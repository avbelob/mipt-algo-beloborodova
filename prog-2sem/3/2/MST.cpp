#include "MST.h"

void VertexProcessing(
    int &vertex, ListGraph &graph_mst, const ListGraph &graph,
    vector<pair<int, double>> &parent,
    priority_queue<pair<double, int>, vector<pair<double, int>>,
                   greater<pair<double, int>>> &distance_to_the_tree) {
  if (vertex != 0) {
    graph_mst.AddEdge(parent[vertex].first, vertex, parent[vertex].second);
    graph_mst.AddEdge(vertex, parent[vertex].first, parent[vertex].second);
  }
  vector<Edge> outgoing_edges;
  graph.GetOutgoingEdges(vertex, outgoing_edges);
  for (Edge edges : outgoing_edges) {
    if (edges.weight <= parent[edges.to].second) {
      parent[edges.to] = std::make_pair(vertex, edges.weight);
    }
    distance_to_the_tree.emplace(edges.weight, edges.to);
  }
}

ListGraph GetMST(const ListGraph &graph) {
  ListGraph graph_mst(graph.GetVerticesCount());
  vector<bool> is_vertices_visited(graph.GetVerticesCount(), false);
  vector<pair<int, double>> parent(graph.GetVerticesCount());
  vector<Edge> outgoing_edges;
  graph.GetOutgoingEdges(0, outgoing_edges);

  for (Edge edges : outgoing_edges) {
    parent[edges.to] = std::make_pair(0, edges.weight);
  }

  priority_queue<pair<double, int>, vector<pair<double, int>>,
                 greater<pair<double, int>>>
      distance_to_the_tree;
  distance_to_the_tree.emplace(0, 0);

  while (!distance_to_the_tree.empty()) {
    int current_vertex = distance_to_the_tree.top().second;

    if (!is_vertices_visited[current_vertex]) {
      is_vertices_visited[current_vertex] = true;
      VertexProcessing(current_vertex, graph_mst, graph, parent,
                       distance_to_the_tree);
    }

    distance_to_the_tree.pop();
  }

  return graph_mst;
}