#include "BruteForce.h"

void GetTSP(const ListGraph &graph, const ListGraph &graph_mst,
            vector<int> &graph_tsp, int current_vertex,
            vector<bool> &is_vertices_visited) {
  is_vertices_visited[current_vertex] = true;
  vector<Edge> outgoing_edges;
  graph_mst.GetOutgoingEdges(current_vertex, outgoing_edges);

  for (Edge edges : outgoing_edges) {
    if (!is_vertices_visited[edges.to]) {
      GetTSP(graph, graph_mst, graph_tsp, edges.to, is_vertices_visited);
      is_vertices_visited[edges.to] = true;
      graph_tsp.emplace_back(edges.to);
    }
  }
}