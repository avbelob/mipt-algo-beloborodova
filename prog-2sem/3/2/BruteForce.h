#include "MST.h"

void GetTSP(const ListGraph &graph, const ListGraph &graph_mst,
            vector<int> &graph_tsp, int current_vertex,
            vector<bool> &is_vertices_visited);
