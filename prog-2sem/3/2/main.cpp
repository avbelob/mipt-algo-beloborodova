#include <iostream>
#include "Check.h"

int main() {
  int max_vertex_count = 0;
  std::cin >> max_vertex_count;
  //По условию: "несколько раз запустите оценку..."
  int some = 0;
  std::cin >> some;

  for (int vertex_count = 2; vertex_count <= max_vertex_count; ++vertex_count) {
    pair<double, double> mean_and_deviation =
        GetMeanAndDeviation(vertex_count, some);
    std::cout << vertex_count << " " << mean_and_deviation.first << " "
              << mean_and_deviation.second << "\n";
  }
}
