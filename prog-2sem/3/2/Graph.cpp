#include "Graph.h"

ListGraph::ListGraph(int vertices_count) : outgoing_edges_(vertices_count) {}

void ListGraph::AddEdge(int from, int to, double weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  outgoing_edges_[from].emplace_back(from, to, weight);
}

int ListGraph::GetVerticesCount() const { return outgoing_edges_.size(); }

void ListGraph::GetOutgoingEdges(int vertex, std::vector<Edge> &edges) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());

  edges.clear();
  edges = outgoing_edges_[vertex];
}

double ListGraph::GetWeight(int from, int to) const {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  double weight = 0;
  vector<Edge> outgoing_edges;
  GetOutgoingEdges(from, outgoing_edges);

  for (Edge edges : outgoing_edges) {
    if (edges.to == to) {
      weight = edges.weight;
    }
  }

  return weight;
}