#include <assert.h>
#include <iostream>
#include <queue>
#include <vector>

using std::greater;
using std::pair;
using std::priority_queue;
using std::vector;

struct Edge {
  int from;
  int to;
  double weight;

  Edge(int from, int to, double weight) : from(from), to(to), weight(weight){};
};

class ListGraph {
 public:
  explicit ListGraph(int vertices_count);

  ~ListGraph() = default;

  void AddEdge(int from, int to, double weight);

  int GetVerticesCount() const;

  void GetOutgoingEdges(int vertex, std::vector<Edge> &edges) const;

  double GetWeight(int from, int to) const;

 private:
  std::vector<vector<Edge>> outgoing_edges_;
};
