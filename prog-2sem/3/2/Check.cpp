#include "Check.h"
#include <cmath>
#include <queue>
#include <random>
#include <utility>

std::default_random_engine generator;
std::normal_distribution<double> distribution(1, 1);

struct Point {
  double first_coordinate;
  double second_coordinate;

  Point(double x, double y) : first_coordinate(x), second_coordinate(y) {}
};

double GetApproximatePathLength(const ListGraph &graph) {
  double approximate_path_length = 0;
  ListGraph graph_mst = GetMST(graph);
  vector<int> graph_tsp;
  vector<bool> is_vertices_visited(graph.GetVerticesCount(), false);
  graph_tsp.emplace_back(0);
  GetTSP(graph, graph_mst, graph_tsp, 0, is_vertices_visited);
  for (int i = 0; i < graph_tsp.size(); ++i) {
    approximate_path_length +=
        graph.GetWeight(graph_tsp[i], graph_tsp[(i + 1) % graph_tsp.size()]);
  }
  return approximate_path_length;
}

void GetCorrectPathLength(const ListGraph &graph, vector<int> *vertex_index,
                          vector<int> *vertex_order,
                          double &correct_path_length) {
  if (vertex_index->empty()) {
    double current_path_length = 0;
    for (int i = 0; i < vertex_order->size(); ++i) {
      current_path_length += graph.GetWeight(
          *(vertex_order->begin() + i),
          *(vertex_order->begin() + (i + 1) % vertex_order->size()));
    }
    if (current_path_length < correct_path_length) {
      correct_path_length = current_path_length;
    }
  } else {
    for (int i = 0; i < vertex_index->size(); ++i) {
      vector<int> new_vertex_order = *vertex_order;
      vector<int> new_vertex_index = *vertex_index;
      new_vertex_order.emplace_back(*(new_vertex_index.begin() + i));
      new_vertex_index.erase(new_vertex_index.begin() + i);
      GetCorrectPathLength(graph, &new_vertex_index, &new_vertex_order,
                           correct_path_length);
    }
  }
}

double GetDistance(Point point1, Point point2) {
  return sqrt(std::pow(point1.first_coordinate - point2.first_coordinate, 2) +
              std::pow(point1.second_coordinate - point2.second_coordinate, 2));
}

double GetApproximationQuality(int vertex_count) {
  ListGraph graph(vertex_count);
  vector<Point> points;

  for (int i = 0; i < vertex_count; ++i) {
    double first_coordinate = distribution(generator);
    double second_coordinate = distribution(generator);
    points.emplace_back(first_coordinate, second_coordinate);
  }

  for (int vertex1 = 0; vertex1 < vertex_count; ++vertex1) {
    for (int vertex2 = vertex1 + 1; vertex2 < vertex_count; ++vertex2) {
      graph.AddEdge(vertex1, vertex2,
                    GetDistance(points[vertex1], points[vertex2]));
      graph.AddEdge(vertex2, vertex1,
                    GetDistance(points[vertex2], points[vertex1]));
    }
  }

  double approximate_path_length = GetApproximatePathLength(graph);
  vector<int> vertex_index;
  vector<int> vertex_order;
  for (int i = 0; i < graph.GetVerticesCount(); ++i) {
    vertex_index.emplace_back(i);
  }
  double correct_path_length = INT32_MAX;
  GetCorrectPathLength(graph, &vertex_index, &vertex_order,
                       correct_path_length);
  return approximate_path_length / correct_path_length;
}

pair<double, double> GetMeanAndDeviation(int vertex_count, int some) {
  double mean = 0;
  double deviation = 0;
  vector<double> approximation_quality;
  for (int i = 0; i < some; ++i) {
    approximation_quality.emplace_back(GetApproximationQuality(vertex_count));
  }
  for (double i : approximation_quality) {
    mean += i;
  }
  mean /= approximation_quality.size();
  for (double i : approximation_quality) {
    deviation += (i - mean) * (i - mean);
  }
  deviation = sqrt(deviation / approximation_quality.size());
  return std::make_pair(mean, deviation);
}
