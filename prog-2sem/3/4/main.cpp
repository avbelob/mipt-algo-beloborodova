/*
Условие:
В одном из отделов крупной организации работает n человек.
Как практически все сотрудники этой организации,
они любят пить чай в перерывах между работой.
При этом они достаточно дисциплинированы и
делают в день ровно один перерыв, во время которого пьют чай.
Для того, чтобы этот перерыв был максимально приятным,
каждый из сотрудников этого отдела обязательно
пьет чай одного из своих любимых сортов.
В разные дни сотрудник может пить чай разных сортов.
Для удобства пронумеруем сорта чая числами от 1 до m.
Недавно сотрудники отдела купили себе большой набор чайных пакетиков,
который содержит a1 пакетиков чая сорта номер 1,
a2 пакетиков чая сорта номер 2, ...,
am пакетиков чая сорта номер m.
Теперь они хотят знать, на какое максимальное число дней
им может хватить купленного набора так,
чтобы в каждый из дней каждому из сотрудников доставался пакетик чая
одного из его любимых сортов.
Каждый сотрудник отдела пьет в день ровно одну чашку чая,
которую заваривает из одного пакетика.
При этом пакетики чая не завариваются повторно.

Входные данные
Первая строка содержит два целых числа n и m (1 ≤ n, m ≤ 50).
Вторая строка содержит m целых чисел a1, ..., am
 (1 ≤ ai ≤ 106 для всех i от 1 до m).
Далее следуют n строк —
i-я из этих строк описывает любимые сорта i-го сотрудника отдела
и имеет следующий формат:
сначала следует положительное число ki — количество любимых сортов
чая этого сотрудника,
а затем идут ki различных чисел от 1 до m — номера этих сортов.

Выходные данные
Выведите одно целое число — искомое максимальное количество дней.

 */
#include <assert.h>
#include <iostream>
#include <queue>
#include <vector>

using std::make_pair;
using std::pair;
using std::queue;
using std::vector;

class MatrixGraph {
 public:
  explicit MatrixGraph(int vertices_count);
  explicit MatrixGraph(const MatrixGraph *graph);

  ~MatrixGraph() = default;

  void AddEdge(int from, int to, int weight);

  int GetVerticesCount() const;

  int GetWeight(int from, int to) const;

  void GetNextVertices(int vertex, vector<int> &next_vertices) const;

  void ReduceWeight(int from, int to, int difference);

  bool HasPath(int vertex_start, int vertex_end,
               vector<pair<int, int>> &parent);

  void GetShortestWay(int vertex_start, int vertex_end,
                      vector<pair<int, int>> &shortest_way);

  int GetMaxFlow(int vertex_start, int vertex_end);

 private:
  std::vector<std::vector<int>> weight_matrix_;
};

MatrixGraph::MatrixGraph(int vertices_count)
    : weight_matrix_(vertices_count, vector<int>(vertices_count, 0)) {}

MatrixGraph::MatrixGraph(const MatrixGraph *graph) {
  weight_matrix_.resize(graph->GetVerticesCount());

  for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
    weight_matrix_[vertex].resize(GetVerticesCount(), 0);
  }

  for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
    std::vector<int> next_vertices;
    graph->GetNextVertices(vertex, next_vertices);

    for (int next_vertex = 0; next_vertex < next_vertices.size();
         ++next_vertex) {
      AddEdge(vertex, next_vertices[next_vertex],
              graph->GetWeight(vertex, next_vertex));
    }
  }
}

void MatrixGraph::AddEdge(int from, int to, int weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  weight_matrix_[from][to] += weight;
}

int MatrixGraph::GetVerticesCount() const { return weight_matrix_.size(); }

int MatrixGraph::GetWeight(int from, int to) const {
  return weight_matrix_[from][to];
}

void MatrixGraph::GetNextVertices(int vertex,
                                  vector<int> &next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  for (int i = 0; i < GetVerticesCount(); ++i) {
    if (weight_matrix_[vertex][i] != 0) {
      next_vertices.emplace_back(i);
    }
  }
}

void MatrixGraph::ReduceWeight(int from, int to, int difference) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  weight_matrix_[from][to] -= difference;
  weight_matrix_[to][from] += difference;
}

bool MatrixGraph::HasPath(int vertex_start, int vertex_end,
                          vector<pair<int, int>> &parent) {
  queue<int> vertices_queue;
  vertices_queue.push(vertex_start);
  vector<bool> is_vertices_visited(GetVerticesCount());
  vector<int> path_length(GetVerticesCount());
  is_vertices_visited[vertex_start] = true;

  while (!vertices_queue.empty()) {
    int current_vertex = vertices_queue.front();
    vertices_queue.pop();
    vector<int> next_vertices;
    GetNextVertices(current_vertex, next_vertices);
    for (int vertex : next_vertices) {
      int weight = GetWeight(current_vertex, vertex);
      if (!is_vertices_visited[vertex] && weight > 0) {
        is_vertices_visited[vertex] = true;
        vertices_queue.push(vertex);
        path_length[vertex] = path_length[current_vertex] + 1;
        parent[vertex] = make_pair(current_vertex, weight);
      }
    }
  }

  return is_vertices_visited[vertex_end];
}
void MatrixGraph::GetShortestWay(int vertex_start, int vertex_end,
                                 vector<pair<int, int>> &shortest_way) {
  vector<pair<int, int>> parent(GetVerticesCount());

  if (HasPath(vertex_start, vertex_end, parent)) {
    int current_vertex = vertex_end;
    while (current_vertex != vertex_start) {
      shortest_way.emplace_back(current_vertex, parent[current_vertex].second);
      current_vertex = parent[current_vertex].first;
    }
  }
}

int MatrixGraph::GetMaxFlow(int vertex_start, int vertex_end) {
  int max_flow = 0;
  vector<pair<int, int>> shortest_way;
  GetShortestWay(vertex_start, vertex_end, shortest_way);

  while (!shortest_way.empty()) {
    int min_bandwidth = INT32_MAX;

    for (auto &vertex : shortest_way) {
      if (vertex.second < min_bandwidth) {
        min_bandwidth = vertex.second;
      }
    }

    max_flow += min_bandwidth;

    for (int vertex = 0; vertex < shortest_way.size() - 1; ++vertex) {
      ReduceWeight(shortest_way[vertex + 1].first, shortest_way[vertex].first,
                   min_bandwidth);
    }

    ReduceWeight(vertex_start, shortest_way[shortest_way.size() - 1].first,
                 min_bandwidth);
    shortest_way.clear();
    GetShortestWay(vertex_start, vertex_end, shortest_way);
  }

  return max_flow;
}

int GetMaxCountOfDays(MatrixGraph &graph, int persons_count,
                      int upper_limit_of_days) {
  int left_bound_answer = 0;
  int right_bound_answer = upper_limit_of_days;

  while (left_bound_answer <= right_bound_answer) {
    int curr_days_count = (left_bound_answer + right_bound_answer) / 2;
    MatrixGraph curr_graph(graph);

    for (int i = 0; i < persons_count; ++i) {
      curr_graph.AddEdge(0, i + 1, curr_days_count);
    }

    if (curr_graph.GetMaxFlow(0, curr_graph.GetVerticesCount() - 1) >=
        curr_days_count * persons_count) {
      left_bound_answer = curr_days_count + 1;
    } else {
      right_bound_answer = curr_days_count - 1;
    }
  }

  return right_bound_answer;
}

int main() {
  int persons_count = 0;
  int variety_tea_count = 0;
  std::cin >> persons_count >> variety_tea_count;
  int vertex_count = persons_count + variety_tea_count + 2;
  MatrixGraph graph(vertex_count);
  int upper_limit_of_days = 0;

  for (int i = 0; i < variety_tea_count; ++i) {
    int tea_count = 0;
    int curr_vertex_from = persons_count + i + 1;
    std::cin >> tea_count;
    upper_limit_of_days += tea_count;
    graph.AddEdge(curr_vertex_from, vertex_count - 1, tea_count);
  }

  for (int i = 0; i < persons_count; ++i) {
    int number_of_favorite_teas_count = 0;
    std::cin >> number_of_favorite_teas_count;
    for (int j = 0; j < number_of_favorite_teas_count; ++j) {
      int variety_of_tea;
      std::cin >> variety_of_tea;
      int curr_vertex_to = persons_count + variety_of_tea;
      graph.AddEdge(i + 1, curr_vertex_to, INT32_MAX);
    }
  }

  upper_limit_of_days += 1;
  std::cout << GetMaxCountOfDays(graph, persons_count, upper_limit_of_days);
}