#include <assert.h>
#include <iostream>
#include <queue>
#include <vector>

using std::vector;
using std::greater;
using std::priority_queue;
using std::pair;
using std::make_pair;

class IGraph {
public:
    virtual ~IGraph() {}

    // Добавление ребра от from к to.
    virtual void AddEdge(int from, int to, int weight) = 0;

    virtual int GetVerticesCount() const = 0;

    virtual int GetWeight(int from, int to) const = 0;

    virtual void GetNextVertices(int vertex,
                                 vector<int>& vertices) const = 0;
    virtual void GetPrevVertices(int vertex,
                                 vector<int>& vertices) const = 0;
};

class MatrixGraph : public IGraph {
public:
    explicit MatrixGraph(int vertices_count);
    ~MatrixGraph() = default;

    virtual void AddEdge(int from, int to, int weight) override final;

    virtual int GetVerticesCount() const override final;

    virtual int GetWeight(int from, int to) const override final;

    virtual void GetNextVertices(int vertex,
                                 vector<int>& vertices) const override final;
    virtual void GetPrevVertices(int vertex,
                                 vector<int>& vertices) const override final;

private:
    std::vector<std::vector<int>> weight_;
};

MatrixGraph::MatrixGraph(int vertices_count) : weight_(vertices_count) {
  for (int i = 0; i < vertices_count; ++i) {
    weight_[i].resize(vertices_count, -1);
  }
}

void MatrixGraph::AddEdge(int from, int to, int weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  if (weight < GetWeight(from, to) || GetWeight(from, to) == -1) {
    weight_[from][to] = weight;
  }
}

int MatrixGraph::GetVerticesCount() const { return weight_.size(); }

int MatrixGraph::GetWeight(int from, int to) const {
  return weight_[from][to];
}

void MatrixGraph::GetNextVertices(int vertex,
                                  vector<int>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  for (int i = 0; i < GetVerticesCount(); ++i) {
    if (weight_[vertex][i] != -1) {
      next_vertices.emplace_back(i);
    }
  }
}

void MatrixGraph::GetPrevVertices(int vertex,
                                  vector<int>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(prev_vertices.empty());

  for (int i = 0; i < GetVerticesCount(); ++i) {
    if (weight_[i][vertex] != -1) {
      prev_vertices.emplace_back(i);
    }
  }
}

int GetWeightOfMST(MatrixGraph& graph) {
  int weight_of_MST = 0;
  vector<bool> is_vertices_visited(graph.GetVerticesCount(), false);
  priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> distance_to_the_tree;
  distance_to_the_tree.emplace(make_pair(0, 0));

  while (!distance_to_the_tree.empty()) {
    int current_vertex = distance_to_the_tree.top().second;
    if (is_vertices_visited[current_vertex]) {
      distance_to_the_tree.pop();
      continue;
    }
    weight_of_MST += distance_to_the_tree.top().first;
    distance_to_the_tree.pop();
    is_vertices_visited[current_vertex] = true;

    vector<int> next_vertices;
    graph.GetNextVertices(current_vertex, next_vertices);

    for (int vertex : next_vertices) {
      distance_to_the_tree.emplace(make_pair(graph.GetWeight(vertex, current_vertex), vertex));
    }
  }

  return weight_of_MST;
}

int main() {
  int vertex_count = 0;
  int edge_count = 0;
  std::cin >> vertex_count >> edge_count;
  MatrixGraph graph(vertex_count);

  for (int i = 0; i < edge_count; ++i) {
    int vertex1 = 0;
    int vertex2 = 0;
    int weight = 0;
    std::cin >> vertex1 >> vertex2 >> weight;
    --vertex1;
    --vertex2;
    graph.AddEdge(vertex1, vertex2, weight);
    graph.AddEdge(vertex2, vertex1, weight);
  }

  std::cout << GetWeightOfMST(graph);
}