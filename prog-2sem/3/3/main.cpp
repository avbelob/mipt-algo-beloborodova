/*
Условие:
Задан ориентированный граф,
каждое ребро которого обладает целочисленной пропускной способностью.
Найдите максимальный поток из вершины с номером 1 в вершину с номером 𝑛.

Вариант 1. С помощью алгоритма Эдмондса-Карпа.

Формат входного файла.
Первая строка входного файла содержит 𝑛 и 𝑚 —
количество вершин и количество ребер графа (2 ≤ 𝑛 ≤ 100, 1 ≤ 𝑚 ≤ 1000).
Следующие 𝑚 строк содержат по три числа: номера вершин,
которые соединяет соответствующее ребро графа и его пропускную способность.
Пропускные способности не превосходят 105.

Формат выходного файла.
В выходной файл выведите одно число —
величину максимального потока из вершины с номером 1 в вершину с номером n.
 */

#include <assert.h>
#include <iostream>
#include <queue>
#include <vector>

using std::make_pair;
using std::pair;
using std::queue;
using std::vector;

class MatrixGraph {
 public:
  explicit MatrixGraph(int vertices_count);

  ~MatrixGraph() = default;

  void AddEdge(int from, int to, int weight);

  int GetVerticesCount() const;

  int GetWeight(int from, int to) const;

  void GetNextVertices(int vertex, vector<int> &next_vertices) const;

  void ReduceWeight(int from, int to, int difference);

 private:
  std::vector<std::vector<int>> weight_;
};

MatrixGraph::MatrixGraph(int vertices_count) : weight_(vertices_count) {
  for (int i = 0; i < vertices_count; ++i) {
    weight_[i].resize(vertices_count, 0);
  }
}

void MatrixGraph::AddEdge(int from, int to, int weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  weight_[from][to] += weight;
}

int MatrixGraph::GetVerticesCount() const { return weight_.size(); }

int MatrixGraph::GetWeight(int from, int to) const { return weight_[from][to]; }

void MatrixGraph::GetNextVertices(int vertex,
                                  vector<int> &next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  for (int i = 0; i < GetVerticesCount(); ++i) {
    if (weight_[vertex][i] != 0) {
      next_vertices.emplace_back(i);
    }
  }
}

void MatrixGraph::ReduceWeight(int from, int to, int difference) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  weight_[from][to] -= difference;
}

bool HasPath(MatrixGraph &graph, int vertex_start, int vertex_end,
             vector<pair<int, int>> &parent) {
  queue<int> vertices_queue;
  vertices_queue.push(vertex_start);
  vector<bool> is_vertices_visited(graph.GetVerticesCount());
  vector<int> path_length(graph.GetVerticesCount());
  is_vertices_visited[vertex_start] = true;

  while (!vertices_queue.empty()) {
    int current_vertex = vertices_queue.front();
    vertices_queue.pop();
    vector<int> next_vertices;
    graph.GetNextVertices(current_vertex, next_vertices);
    for (int vertex : next_vertices) {
      int weight = graph.GetWeight(current_vertex, vertex);
      if (!is_vertices_visited[vertex] && weight > 0) {
        is_vertices_visited[vertex] = true;
        vertices_queue.push(vertex);
        path_length[vertex] = path_length[current_vertex] + 1;
        parent[vertex] = make_pair(current_vertex, weight);
      }
    }
  }

  return is_vertices_visited[vertex_end];
}
void GetShortestWay(MatrixGraph &graph, int vertex_start, int vertex_end,
                    vector<pair<int, int>> &shortest_way) {
  vector<pair<int, int>> parent(graph.GetVerticesCount());

  if (HasPath(graph, vertex_start, vertex_end, parent)) {
    int current_vertex = vertex_end;
    while (current_vertex != vertex_start) {
      shortest_way.emplace_back(current_vertex, parent[current_vertex].second);
      current_vertex = parent[current_vertex].first;
    }
  }
}

int GetMaxFlow(MatrixGraph &graph, int vertex_start, int vertex_end) {
  int max_flow = 0;
  vector<pair<int, int>> shortest_way;
  GetShortestWay(graph, vertex_start, vertex_end, shortest_way);

  while (!shortest_way.empty()) {
    int min_bandwidth = INT32_MAX;
    for (auto &vertex : shortest_way) {
      if (vertex.second < min_bandwidth) {
        min_bandwidth = vertex.second;
      }
    }
    max_flow += min_bandwidth;
    for (int vertex = 0; vertex < shortest_way.size() - 1; ++vertex) {
      graph.ReduceWeight(shortest_way[vertex + 1].first,
                         shortest_way[vertex].first, min_bandwidth);
      graph.ReduceWeight(shortest_way[vertex].first,
                         shortest_way[vertex + 1].first, -min_bandwidth);
    }
    graph.ReduceWeight(vertex_start,
                       shortest_way[shortest_way.size() - 1].first,
                       min_bandwidth);
    graph.ReduceWeight(shortest_way[shortest_way.size() - 1].first,
                       vertex_start, -min_bandwidth);
    shortest_way.clear();
    GetShortestWay(graph, vertex_start, vertex_end, shortest_way);
  }

  return max_flow;
}

int main() {
  int vertex_count;
  int edge_count;
  std::cin >> vertex_count;
  MatrixGraph graph(vertex_count);
  std::cin >> edge_count;
  for (int i = 0; i < edge_count; ++i) {
    int vertex1;
    int vertex2;
    int weight;
    std::cin >> vertex1 >> vertex2 >> weight;
    --vertex1;
    --vertex2;
    graph.AddEdge(vertex1, vertex2, weight);
  }

  std::cout << GetMaxFlow(graph, 0, vertex_count - 1);
}