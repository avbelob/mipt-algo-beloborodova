/*
Условие:
Необходимо написать торгового советника для поиска арбитража.

Определение
Арбитраж - это торговля по цепочке различных валют в надежде заработать
на небольших различиях в коэффициентах.
Например, есть следующие курсы валют (на 03.05.2015):
GBP/USD: 0.67
RUB/GBP: 78.66
USD/RUB: 0.02
Имея 1$ и совершив цикл USD->GBP->RUB->USD, получим 1.054$.
Таким образом заработав 5.4%.

Время работы – O(VE).

Формат входных данных.
Первая строка содержит число N – количество возможных валют
(размер таблицы котировок)
Далее следует построчное представление таблицы.
Диагональные элементы (i, i) пропущены
(подразумевается, что курс валюты к себе же 1.0)
В элементе таблицы (i, j) содержится обменный курс i->j.
Если обмен в данном направлении не производится, то -1.

Формат выходных данных.
Выведите YES, если арбитраж есть, и NO, иначе.



 */
#include <assert.h>
#include <iostream>
#include <queue>
#include <vector>

class IGraph {
 public:
  virtual ~IGraph() {}

  // Добавление ребра от from к to.
  virtual void AddEdge(int from, int to, double weight) = 0;

  virtual int GetVerticesCount() const = 0;

  virtual double GetWeight(int from, int to) const = 0;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
};

class MatrixGraph : public IGraph {
 public:
  explicit MatrixGraph(int vertices_count);
  explicit MatrixGraph(const IGraph* graph);
  ~MatrixGraph();

  virtual void AddEdge(int from, int to, double weight) override final;

  virtual int GetVerticesCount() const override final;

  virtual double GetWeight(int from, int to) const override final;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const override final;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int>& vertices) const override final;

 private:
  std::vector<std::vector<double>> weight_;
};

MatrixGraph::MatrixGraph(int vertices_count) : weight_(vertices_count) {
  for (int i = 0; i < vertices_count; ++i) {
    weight_[i].resize(vertices_count, -1);
  }
}

MatrixGraph::~MatrixGraph() {}

void MatrixGraph::AddEdge(int from, int to, double weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  weight_[from][to] = weight;
}

int MatrixGraph::GetVerticesCount() const { return weight_.size(); }

double MatrixGraph::GetWeight(int from, int to) const {
  return weight_[from][to];
}

void MatrixGraph::GetNextVertices(int vertex,
                                  std::vector<int>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  for (int i = 0; i < GetVerticesCount(); ++i) {
    if (weight_[vertex][i] != -1) {
      next_vertices.emplace_back(i);
    }
  }
}

void MatrixGraph::GetPrevVertices(int vertex,
                                  std::vector<int>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(prev_vertices.empty());

  for (int i = 0; i < GetVerticesCount(); ++i) {
    if (weight_[i][vertex] != -1) {
      prev_vertices.emplace_back(i);
    }
  }
}

void ParentProcessing(MatrixGraph& graph, int current_vertex,
                      std::vector<double>& max_path_length) {
  std::vector<int> prev_vertices;
  graph.GetPrevVertices(current_vertex, prev_vertices);
  for (int parent : prev_vertices) {
    if ((max_path_length[parent] != -1) &&
        (max_path_length[current_vertex] <
         max_path_length[parent] * graph.GetWeight(parent, current_vertex))) {
      max_path_length[current_vertex] =
          max_path_length[parent] * graph.GetWeight(parent, current_vertex);
    }
  }
}

bool IsThereArbitage(MatrixGraph& graph) {
  std::vector<bool> is_vertices_visited(graph.GetVerticesCount(), false);
  std::vector<double> max_path_length(graph.GetVerticesCount(), -1);
  max_path_length[0] = 1;

  for (int j = 0; j < graph.GetVerticesCount() - 1; ++j) {
    for (int current_vertex = 0; current_vertex < graph.GetVerticesCount();
         ++current_vertex) {
      if (!is_vertices_visited[current_vertex]) {
        is_vertices_visited[current_vertex] = true;
        ParentProcessing(graph, current_vertex, max_path_length);
      }
    }
  }
  for (int current_vertex = 0; current_vertex < graph.GetVerticesCount();
       ++current_vertex) {
    std::vector<int> prev_vertices;
    graph.GetPrevVertices(current_vertex, prev_vertices);
    for (int parent : prev_vertices) {
      if ((max_path_length[parent] != -1) &&
          (max_path_length[current_vertex] <
           max_path_length[parent] * graph.GetWeight(parent, current_vertex))) {
        return true;
      }
    }
  }
  return false;
}

int main() {
  int vertex_count;
  std::cin >> vertex_count;
  MatrixGraph graph(vertex_count);

  for (int vertex1 = 0; vertex1 < vertex_count; ++vertex1) {
    for (int vertex2 = 0; vertex2 < vertex_count; ++vertex2) {
      if (vertex1 != vertex2) {
        double weight;
        std::cin >> weight;
        graph.AddEdge(vertex1, vertex2, weight);
      }
    }
  }

  if (IsThereArbitage(graph)) {
    std::cout << "YES";
  } else {
    std::cout << "NO";
  }
}