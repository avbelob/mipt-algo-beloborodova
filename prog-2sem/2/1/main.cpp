/*
Условие:
Требуется отыскать самый короткий маршрут между городами.
Из города может выходить дорога, которая возвращается в этот же город.

Требуемое время работы O((N + M)log N),
где N – количество городов, M – известных дорог между ними.
N ≤ 10000, M ≤ 250000.
Длина каждой дороги ≤ 10000.

Формат ввода
Первая строка содержит число N – количество городов.

Вторая строка содержит число M - количество дорог.

Каждая следующая строка содержит описание дороги (откуда, куда, время в пути).
Все указанные дороги двусторонние.
Между любыми двумя городами может быть больше одной дороги.

Последняя строка содержит маршрут (откуда и куда нужно доехать).

Формат вывода
Вывести длину самого короткого маршрута.


 */
#include <assert.h>
#include <iostream>
#include <queue>
#include <vector>

class IGraph {
 public:
  virtual ~IGraph() {}

  // Добавление ребра от from к to.
  virtual void AddEdge(int from, int to, int weight) = 0;

  virtual int GetVerticesCount() const = 0;

  virtual void GetNextVertices(
      int vertex, std::vector<std::pair<int, int>>& vertices) const = 0;
  virtual void GetPrevVertices(
      int vertex, std::vector<std::pair<int, int>>& vertices) const = 0;
};

class ListGraph : public IGraph {
 public:
  explicit ListGraph(int vertices_count);
  explicit ListGraph(const IGraph* graph);
  ~ListGraph();

  virtual void AddEdge(int from, int to, int weight) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetNextVertices(
      int vertex,
      std::vector<std::pair<int, int>>& vertices) const override final;
  virtual void GetPrevVertices(
      int vertex,
      std::vector<std::pair<int, int>>& vertices) const override final;

 private:
  std::vector<std::vector<std::pair<int, int>>> next_vertices_;
  std::vector<std::vector<std::pair<int, int>>> prev_vertices_;
};

ListGraph::ListGraph(int vertices_count)
    : next_vertices_(vertices_count), prev_vertices_(vertices_count) {}

ListGraph::ListGraph(const IGraph* graph) {
  next_vertices_.resize(graph->GetVerticesCount());
  prev_vertices_.resize(graph->GetVerticesCount());

  for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
    graph->GetNextVertices(vertex, next_vertices_[vertex]);
    graph->GetPrevVertices(vertex, prev_vertices_[vertex]);
  }
}

ListGraph::~ListGraph() {}

void ListGraph::AddEdge(int from, int to, int weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  next_vertices_[from].emplace_back(to, weight);
  prev_vertices_[to].emplace_back(from, weight);
}

int ListGraph::GetVerticesCount() const { return next_vertices_.size(); }

void ListGraph::GetNextVertices(
    int vertex, std::vector<std::pair<int, int>>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  next_vertices = next_vertices_[vertex];
}

void ListGraph::GetPrevVertices(
    int vertex, std::vector<std::pair<int, int>>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(prev_vertices.empty());

  prev_vertices = prev_vertices_[vertex];
}

int GetShortestPath(ListGraph& graph, int vertex_start, int vertex_end) {
  std::vector<bool> is_vertices_visited(graph.GetVerticesCount(), false);
  std::vector<int> min_path_length(graph.GetVerticesCount(), INT32_MAX);
  min_path_length[vertex_start] = 0;
  std::queue<std::pair<int, int>> vertices_and_length_queue;
  vertices_and_length_queue.emplace(vertex_start, 0);

  while (!vertices_and_length_queue.empty()) {
    int current_vertex = vertices_and_length_queue.front().first;
    is_vertices_visited[current_vertex] = true;
    vertices_and_length_queue.pop();

    std::vector<std::pair<int, int>> next_vertices;
    graph.GetNextVertices(current_vertex, next_vertices);

    for (std::pair<int, int> vertex : next_vertices) {
      if (min_path_length[vertex.first] >
          min_path_length[current_vertex] + vertex.second) {
        min_path_length[vertex.first] =
            min_path_length[current_vertex] + vertex.second;
        vertices_and_length_queue.emplace(vertex.first,
                                          min_path_length[vertex.first]);
      }
    }
  }
  return min_path_length[vertex_end];
}

int main() {
  int vertex_count;
  int edge_count;
  std::cin >> vertex_count;
  ListGraph graph(vertex_count);
  std::cin >> edge_count;

  for (int i = 0; i < edge_count; i++) {
    int vertex1;
    int vertex2;
    int weight;
    std::cin >> vertex1 >> vertex2 >> weight;
    graph.AddEdge(vertex1, vertex2, weight);
    graph.AddEdge(vertex2, vertex1, weight);
  }

  int vertex_start;
  int vertex_end;
  std::cin >> vertex_start >> vertex_end;
  std::cout << GetShortestPath(graph, vertex_start, vertex_end);
}