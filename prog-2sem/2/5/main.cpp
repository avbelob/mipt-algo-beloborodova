/*
Условие:
Полный ориентированный взвешенный граф задан матрицей смежности.
Постройте матрицу кратчайших путей между его вершинами.
Гарантируется, что в графе нет циклов отрицательного веса.

Формат входного файла
В первой строке вводится единственное число 𝑁 (1 ≤ 𝑁 ≤ 100)
— количество вершин графа.
В следующих 𝑁 строках по 𝑁 чисел задается матрица смежности графа
(𝑗-ое число в 𝑖-ой строке — вес ребра из вершины 𝑖 в вершину 𝑗).
Все числа по модулю не превышают 100.
На главной диагонали матрицы — всегда нули.

Формат выходного файла
Выведите 𝑁 строк по 𝑁 чисел
— матрицу расстояний между парами вершин,
где 𝑗-ое число в 𝑖-ой строке равно весу кратчайшего пути из вершины 𝑖 в j.

 */

#include <assert.h>
#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>

class IGraph {
 public:
  virtual ~IGraph() {}

  // Добавление ребра от from к to.
  virtual void AddEdge(int from, int to, int weight) = 0;

  virtual int GetVerticesCount() const = 0;
};

class MatrixGraph : public IGraph {
 public:
  explicit MatrixGraph(int vertices_count);
  ~MatrixGraph();

  virtual void AddEdge(int from, int to, int weight) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetMatrixShortestWays(
      std::vector<std::vector<int>>& matrix_shortest_way) const;

 private:
  std::vector<std::vector<int>> weight_;
};

MatrixGraph::MatrixGraph(int vertices_count) : weight_(vertices_count) {
  for (int i = 0; i < vertices_count; ++i) {
    weight_[i].resize(vertices_count);
  }
}

MatrixGraph::~MatrixGraph() {}

void MatrixGraph::AddEdge(int from, int to, int weight) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  weight_[from][to] = weight;
}

int MatrixGraph::GetVerticesCount() const { return weight_.size(); }

void MatrixGraph::GetMatrixShortestWays(
    std::vector<std::vector<int>>& matrix_shortest_way) const {
  matrix_shortest_way = weight_;
  for (int k = 0; k < GetVerticesCount(); ++k) {
    for (int vertex1 = 0; vertex1 < GetVerticesCount(); ++vertex1) {
      for (int vertex2 = 0; vertex2 < GetVerticesCount(); ++vertex2) {
        matrix_shortest_way[vertex1][vertex2] = std::min(
            matrix_shortest_way[vertex1][vertex2],
            matrix_shortest_way[vertex1][k] + matrix_shortest_way[k][vertex2]);
      }
    }
  }
}

int main() {
  int vertex_count;
  std::cin >> vertex_count;
  MatrixGraph graph(vertex_count);

  for (int vertex1 = 0; vertex1 < vertex_count; ++vertex1) {
    for (int vertex2 = 0; vertex2 < vertex_count; ++vertex2) {
      int weight;
      std::cin >> weight;
      graph.AddEdge(vertex1, vertex2, weight);
    }
  }

  std::vector<std::vector<int>> matrix_shortest_way;
  graph.GetMatrixShortestWays(matrix_shortest_way);

  for (int vertex1 = 0; vertex1 < vertex_count; ++vertex1) {
    for (int vertex2 = 0; vertex2 < vertex_count; ++vertex2) {
      std::cout << matrix_shortest_way[vertex1][vertex2] << " ";
    }
    std::cout << "\n";
  }
}