/*
Условие:
«Восьминашки» – упрощенный вариант известной головоломки «Пятнашки».
Восемь костяшек, пронумерованных от 1 до 8,
расставлены по ячейкам игровой доски 3 на 3,
одна ячейка при этом остается пустой.
За один ход разрешается передвинуть одну из костяшек,
расположенных рядом с пустой ячейкой, на свободное место.
Цель игры – для заданной начальной конфигурации игровой доски за
минимальное число ходов получить выигрышную конфигурацию
(пустая ячейка обозначена нулем):
1 2 3
4 5 6
7 8 0
Формат входного файла
Во входном файле содержится начальная конфигурация головоломки –
3 строки по 3 числа в каждой.
Формат выходного файла
Если решение существует, то в первой строке выходного файла выведите
минимальное число перемещений костяшек, которое нужно сделать,
чтобы достичь выигрышной конфигурации,
а во второй строке выведите соответствующую последовательность ходов:
L означает, что в результате перемещения
костяшки пустая ячейка сдвинулась влево,
R – вправо, U – вверх, D – вниз.
Если таких последовательностей несколько,
то выведите любую из них.
Если же выигрышная конфигурация недостижима,
то выведите в выходной файл одно число −1.

 */
#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <vector>

const int max_heuristic = 32;

struct Condition {
  Condition(std::vector<std::vector<int>> new_table,
            std::pair<int, int> new_coordinates, int new_steps,
            std::string new_sequence_of_moves) {
    table = new_table;
    coordinates = new_coordinates;
    num_of_steps = new_steps;
    sequence_of_moves = new_sequence_of_moves;
  }
  ~Condition() {}
  std::pair<int, int> coordinates;
  int num_of_steps;
  std::vector<std::vector<int>> table;
  std::string sequence_of_moves;
};

int CountInversions(std::vector<std::vector<int>>& table) {
  int inversions_count = 0;
  for (int first_coordinate = 0; first_coordinate < 3; ++first_coordinate) {
    for (int second_coordinate = 0; second_coordinate < 3;
         ++second_coordinate) {
      int current_pos = table[first_coordinate][second_coordinate];
      for (int i = first_coordinate; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
          if ((i > first_coordinate ||
               ((i == first_coordinate) && (j > second_coordinate))) &&
              (current_pos != 0) && (table[i][j] != 0) &&
              (table[i][j] < current_pos)) {
            ++inversions_count;
          }
        }
      }
    }
  }
}

bool IsExistenceSolution(std::vector<std::vector<int>>& table) {
  int inversions_count = CountInversions(table);
  return inversions_count % 2 == 0;
}

int GetHeurictics(std::vector<std::vector<int>>& table) {
  int heurictics = 0;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (table[i][j] != 0) {
        heurictics +=
            abs(j - (table[i][j] - 1) % 3) + abs(i - (table[i][j] - 1) / 3);
      }
    }
  }
  return heurictics;
}

std::pair<int, int> GetEmptyCell(std::vector<std::vector<int>>& table) {
  std::pair<int, int> coordinates;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (table[i][j] == 0) {
        coordinates.first = i;
        coordinates.second = j;
      }
    }
  }
  return coordinates;
}

void GoUp(std::vector<std::vector<int>>& table, Condition cur_pos,
          std::queue<Condition>& queue_for_bfs) {
  std::swap(
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second],
      cur_pos.table[cur_pos.coordinates.first - 1][cur_pos.coordinates.second]);
  std::pair<int, int> new_coordinates =
      std::make_pair(cur_pos.coordinates.first - 1, cur_pos.coordinates.second);
  Condition pushing_pos(cur_pos.table, new_coordinates,
                        cur_pos.num_of_steps + 1,
                        cur_pos.sequence_of_moves + 'U');
  queue_for_bfs.emplace(pushing_pos);
  std::swap(
      cur_pos.table[cur_pos.coordinates.first - 1][cur_pos.coordinates.second],
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second]);
}
void GoDown(std::vector<std::vector<int>>& table, Condition cur_pos,
            std::queue<Condition>& queue_for_bfs) {
  std::swap(
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second],
      cur_pos.table[cur_pos.coordinates.first + 1][cur_pos.coordinates.second]);
  std::pair<int, int> new_coordinates =
      std::make_pair(cur_pos.coordinates.first + 1, cur_pos.coordinates.second);
  Condition pushing_pos(cur_pos.table, new_coordinates,
                        cur_pos.num_of_steps + 1,
                        cur_pos.sequence_of_moves + 'D');
  queue_for_bfs.emplace(pushing_pos);
  std::swap(
      cur_pos.table[cur_pos.coordinates.first + 1][cur_pos.coordinates.second],
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second]);
}
void GoLeft(std::vector<std::vector<int>>& table, Condition cur_pos,
            std::queue<Condition>& queue_for_bfs) {
  std::swap(
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second],
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second - 1]);
  std::pair<int, int> new_coordinates =
      std::make_pair(cur_pos.coordinates.first, cur_pos.coordinates.second - 1);
  Condition pushing_pos(cur_pos.table, new_coordinates,
                        cur_pos.num_of_steps + 1,
                        cur_pos.sequence_of_moves + 'L');
  queue_for_bfs.emplace(pushing_pos);
  std::swap(
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second - 1],
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second]);
}
void GoRight(std::vector<std::vector<int>>& table, Condition cur_pos,
             std::queue<Condition>& queue_for_bfs) {
  std::swap(
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second],
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second + 1]);
  std::pair<int, int> new_coordinates =
      std::make_pair(cur_pos.coordinates.first, cur_pos.coordinates.second + 1);
  Condition pushing_pos(cur_pos.table, new_coordinates,
                        cur_pos.num_of_steps + 1,
                        cur_pos.sequence_of_moves + 'R');
  queue_for_bfs.emplace(pushing_pos);
  std::swap(
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second + 1],
      cur_pos.table[cur_pos.coordinates.first][cur_pos.coordinates.second]);
}
void MakeStep(std::vector<std::vector<int>>& table, Condition cur_pos,
              std::queue<Condition>& queue_for_bfs) {
  if ((cur_pos.coordinates.first != 0) &&
      ((cur_pos.sequence_of_moves.empty()) ||
       (cur_pos.sequence_of_moves[cur_pos.sequence_of_moves.size() - 1] !=
        'D'))) {
    GoUp(table, cur_pos, queue_for_bfs);
  }
  if ((cur_pos.coordinates.first != 2) &&
      ((cur_pos.sequence_of_moves.empty()) ||
       (cur_pos.sequence_of_moves[cur_pos.sequence_of_moves.size() - 1] !=
        'U'))) {
    GoDown(table, cur_pos, queue_for_bfs);
  }
  if ((cur_pos.coordinates.second != 0) &&
      ((cur_pos.sequence_of_moves.empty()) ||
       (cur_pos.sequence_of_moves[cur_pos.sequence_of_moves.size() - 1] !=
        'R'))) {
    GoLeft(table, cur_pos, queue_for_bfs);
  }
  if ((cur_pos.coordinates.second != 2) &&
      ((cur_pos.sequence_of_moves.empty()) ||
       (cur_pos.sequence_of_moves[cur_pos.sequence_of_moves.size() - 1] !=
        'L'))) {
    GoRight(table, cur_pos, queue_for_bfs);
  }
}

std::string SequenceOfMoves(std::vector<std::vector<int>>& table) {
  std::string sequence_of_moves;

  std::pair<int, int> coordinates_empty_vertex;
  coordinates_empty_vertex = GetEmptyCell(table);

  std::queue<Condition> queue_for_bfs;
  Condition start(table, coordinates_empty_vertex, 0, "");
  queue_for_bfs.emplace(start);

  while (!queue_for_bfs.empty()) {
    Condition cur_pos = queue_for_bfs.front();
    queue_for_bfs.pop();
    int heuristic = GetHeurictics(cur_pos.table);
    if (heuristic == 0) {
      sequence_of_moves = cur_pos.sequence_of_moves;
      return sequence_of_moves;
    }
    if (cur_pos.num_of_steps + heuristic <= max_heuristic) {
      MakeStep(table, cur_pos, queue_for_bfs);
    }
  }
  return sequence_of_moves;
}

int main() {
  std::vector<std::vector<int>> table(3, std::vector<int>(3));
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      int value;
      std::cin >> value;
      table[i][j] = value;
    }
  }
  if (!IsExistenceSolution(table)) {
    std::cout << "-1";
  } else {
    std::string sequence_of_moves;
    sequence_of_moves = SequenceOfMoves(table);
    std::cout << sequence_of_moves.length() << "\n";
    for (int i = 0; i < sequence_of_moves.length(); ++i) {
      std::cout << sequence_of_moves[i];
    }
  }
}
