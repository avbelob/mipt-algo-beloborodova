/*
Условие:
Дан невзвешенный неориентированный граф.
В графе может быть несколько кратчайших путей между какими-то вершинами.
Найдите количество различных кратчайших путей между заданными вершинами.
Требуемая сложность O(V+E).
Ввод: v:кол-во вершин(макс. 50000), n:кол-во ребер(макс. 200000),
n пар реберных вершин, пара вершин v, w для запроса.
Вывод:количество кратчайших путей от v к w.
 */

#include <iostream>
#include <queue>
#include <vector>

using std::queue;
using std::vector;

class Graph {
 public:
  Graph(unsigned int vertex_count)
      : vertex_count_(vertex_count), buffer_(vertex_count_) {}

  void AddEdge(int from, int to);


  int GetTheNumberOfPath(int vertex_start, int vertex_end);

 private:
  int BFS(int vertex_start, int vertex_end);

  int vertex_count_;

  vector<vector<int> > buffer_;
};

void Graph::AddEdge(int from, int to) {
  buffer_[from].push_back(to);
  buffer_[to].push_back(from);
}

int Graph::BFS(int vertex_start, int vertex_end) {
  queue<int> vertices_queue;
  vertices_queue.push(vertex_start);
  vector<bool> is_vertices_visited(vertex_count_);
  vector<int> path_length(vertex_count_);
  vector<int> number_of_path(vertex_count_);
  number_of_path[vertex_start] = 1;
  is_vertices_visited[vertex_start] = true;
  while (!vertices_queue.empty()) {
    int current_vertex = vertices_queue.front();
    vertices_queue.pop();
    for (size_t i = 0; i < buffer_[current_vertex].size(); ++i) {
      int next_vertices = buffer_[current_vertex][i];
      if (!is_vertices_visited[next_vertices]) {
        number_of_path[next_vertices] = number_of_path[current_vertex];
        is_vertices_visited[next_vertices] = true;
        vertices_queue.push(next_vertices);
        path_length[next_vertices] = path_length[current_vertex] + 1;
      } else if (path_length[next_vertices] ==
                 path_length[current_vertex] + 1) {
        number_of_path[next_vertices] += number_of_path[current_vertex];
      }
    }
  }
  return number_of_path[vertex_end];
}

int Graph::GetTheNumberOfPath(int vertex_start, int vertex_end) {
  return BFS(vertex_start, vertex_end);
}

int main() {
  int vertex_count;
  int edge_count;
  std::cin >> vertex_count;
  Graph graph(vertex_count);
  std::cin >> edge_count;

  for (int i = 0; i < edge_count; i++) {
    int vertex1;
    int vertex2;
    std::cin >> vertex1 >> vertex2;
    graph.AddEdge(vertex1, vertex2);
  }

  int vertex_start;
  int vertex_end;
  std::cin >> vertex_start >> vertex_end;
  std::cout << graph.GetTheNumberOfPath(vertex_start, vertex_end);
}