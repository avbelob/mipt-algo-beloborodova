/*
Условие:
Дан ориентированный граф.
Определите, какое минимальное число ребер необходимо добавить,
чтобы граф стал сильносвязным.
В графе возможны петли.

Формат ввода:
В первой строке указывается число вершин графа V,
во второй – число ребер E,
в последующих – E пар вершин, задающих ребра.

Формат вывода:
Минимальное число ребер k.
 */
#include <assert.h>
#include <iostream>
#include <stack>
#include <vector>
class IGraph {
 public:
  virtual ~IGraph() {}

  // Добавление ребра от from к to.
  virtual void AddEdge(int from, int to) = 0;

  virtual int GetVerticesCount() const = 0;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
  virtual void GetPrevVertices(int vertex,
							   std::vector<int>& vertices) const = 0;

  virtual void DFS(int vertex) = 0;
};

class ListGraph : public IGraph {
 public:
  explicit ListGraph(int vertices_count);
  ~ListGraph();

  virtual void AddEdge(int from, int to) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetNextVertices(int vertex,
							   std::vector<int>& vertices) const override final;
  virtual void GetPrevVertices(int vertex,
							   std::vector<int>& vertices) const override final;

  virtual void DFS(int vertex) override final;

  friend ListGraph CreateSimpllifiedGraph(ListGraph graph);

 private:
  int current_time_ = 0;
  int current_component_ = 0;
  std::vector<std::vector<int>> next_vertices_;
  std::vector<std::vector<int>> prev_vertices_;
  std::vector<bool> visited_;
  std::stack<int> stack_for_tarjan_;
  std::vector<int> lowest_vertex_;
  std::vector<int> vertex_components_;
};

ListGraph::ListGraph(int vertices_count)
	: next_vertices_(vertices_count), prev_vertices_(vertices_count) {
  visited_.resize(vertices_count);
  visited_.assign(vertices_count, false);
  vertex_components_.assign(vertices_count, 0);
  visited_.assign(vertices_count, false);
  lowest_vertex_.assign(vertices_count, 0);
}

ListGraph::~ListGraph() {}

void ListGraph::AddEdge(int from, int to) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());
  next_vertices_[from].emplace_back(to);
  prev_vertices_[to].emplace_back(from);
}

int ListGraph::GetVerticesCount() const { return next_vertices_.size(); }

void ListGraph::GetNextVertices(int vertex,
								std::vector<int>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  next_vertices.clear();
  next_vertices = next_vertices_[vertex];
}

void ListGraph::GetPrevVertices(int vertex,
								std::vector<int>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  prev_vertices.clear();
  prev_vertices = prev_vertices_[vertex];
}

void ListGraph::DFS(int vertex) {
  visited_[vertex] = true;
  stack_for_tarjan_.push(vertex);
  lowest_vertex_[vertex] = current_time_;
  ++current_time_;
  bool is_vertex_root = true;
  std::vector<int> next_vertices;
  GetNextVertices(vertex, next_vertices);

  for (int i : next_vertices) {
    if (!visited_[i]) {
      DFS(i);
    }
    if (lowest_vertex_[i] < lowest_vertex_[vertex]) {
      lowest_vertex_[vertex] = lowest_vertex_[i];
      is_vertex_root = false;
    }
  }

  if (is_vertex_root) {
    while (true) {
      int current_vertex = stack_for_tarjan_.top();
      stack_for_tarjan_.pop();
      vertex_components_[current_vertex] = current_component_;
      lowest_vertex_[current_vertex] = GetVerticesCount() + 1;
      if (vertex == current_vertex) {
        break;
      }
    }
    ++current_component_;
  }
}

ListGraph CreateSimpllifiedGraph(ListGraph graph) {
  for (int vertex = 0; vertex < graph.GetVerticesCount(); ++vertex) {
    if (!graph.visited_[vertex]) {
      graph.DFS(vertex);
    }
  }
  ListGraph current_graph(graph.current_component_);
  for (int vertex = 0; vertex < graph.GetVerticesCount(); ++vertex) {
    std::vector<int> next_vertices;
    graph.GetNextVertices(vertex, next_vertices);
    for (int i : next_vertices) {
      if (graph.vertex_components_[vertex] != graph.vertex_components_[i]) {
        current_graph.AddEdge(graph.vertex_components_[i],
                              graph.vertex_components_[vertex]);
      }
    }
  }
  return current_graph;
}

int main() {
  int vertex_count;
  int edge_count;
  std::cin >> vertex_count;
  ListGraph graph(vertex_count);
  std::cin >> edge_count;
  for (int i = 0; i < edge_count; i++) {
    int vertex1;
    int vertex2;
    std::cin >> vertex1 >> vertex2;
    graph.AddEdge(vertex1 - 1, vertex2 - 1);
  }

  ListGraph simpllified_graph = CreateSimpllifiedGraph(graph);
  int vertex_without_out_count = 0;
  int vertex_without_in_count = 0;

  if (simpllified_graph.GetVerticesCount() > 1) {
    for (int i = 0; i < simpllified_graph.GetVerticesCount(); ++i) {
	  std::vector<int> next_vertices;
	  simpllified_graph.GetNextVertices(i, next_vertices);
	  if (next_vertices.empty()) {
	    ++vertex_without_out_count;
	  }
	  std::vector<int> prev_vertices;
	  simpllified_graph.GetPrevVertices(i, prev_vertices);
	  if (prev_vertices.empty()) {
	    ++vertex_without_in_count;
	  }
    }
  }
  std::cout << std::max(vertex_without_in_count, vertex_without_out_count);
}