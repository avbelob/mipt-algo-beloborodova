/*
Условие:
Дан невзвешенный неориентированный граф.
Определить, является ли он двудольным.
Требуемая сложность O(V+E).
Ввод: v:кол-во вершин(макс. 50000), n:кол-во ребер(макс. 200000),
n пар реберных вершин.
Вывод: YES если граф является двудольным, NO - если не является.
*/
#include <iostream>
#include <queue>
#include <vector>

using std::queue;
using std::vector;

class Graph {
 public:
  Graph(unsigned int vertex_count)
      : vertex_count_(vertex_count), buffer_(vertex_count_) {}

  void AddEdge(int from, int to);

  bool IsGraphBipartite();

 private:
  bool BFS();

  int vertex_count_;

  vector<vector<int> > buffer_;
};

void Graph::AddEdge(int from, int to) {
  buffer_[from].push_back(to);
  buffer_[to].push_back(from);
}

bool Graph::BFS() {
  queue<int> vertices_queue;
  vertices_queue.push(0);
  vector<bool> is_vertices_visited(vertex_count_);
  vector<int> path_length(vertex_count_);
  vector<bool> is_vertex_even(vertex_count_);
  is_vertex_even[0] = true;
  is_vertices_visited[0] = true;

  while (!vertices_queue.empty()) {
    int current_vertex = vertices_queue.front();
    vertices_queue.pop();
    for (size_t i = 0; i < buffer_[current_vertex].size(); ++i) {
      int next_vertices = buffer_[current_vertex][i];
      if (!is_vertices_visited[next_vertices]) {
        is_vertex_even[next_vertices] = !is_vertex_even[current_vertex];
        is_vertices_visited[next_vertices] = true;
        vertices_queue.push(next_vertices);
        path_length[next_vertices] = path_length[current_vertex] + 1;
      } else if (is_vertex_even[next_vertices] ==
                 is_vertex_even[current_vertex]) {
        return false;
      }
    }
  }
  return true;
}

bool Graph::IsGraphBipartite() { return BFS(); }

int main() {
  int vertex_count;
  int edge_count;
  std::cin >> vertex_count;
  Graph graph(vertex_count);
  std::cin >> edge_count;

  for (int i = 0; i < edge_count; i++) {
    int vertex1;
    int vertex2;
    std::cin >> vertex1 >> vertex2;
    graph.AddEdge(vertex1, vertex2);
  }
  if (graph.IsGraphBipartite()) {
    std::cout << "YES";
  } else {
    std::cout << "NO";
  }
}