#include "ArcGraph.h"
#include <assert.h>

ArcGraph::ArcGraph(int vertices_count) : vertices_count_(vertices_count) {}

ArcGraph::ArcGraph(const IGraph* graph) {
  vertices_count_ = graph->GetVerticesCount();
  for (int vertex = 0; vertex < vertices_count_; ++vertex) {
    std::vector<int> next_vertices;
    graph->GetNextVertices(vertex, next_vertices);
    for (int next_vertex = 0; next_vertex < next_vertices.size();
         ++next_vertex) {
      AddEdge(vertex, next_vertices[next_vertex]);
    }
  }
}

ArcGraph::~ArcGraph() {}

void ArcGraph::AddEdge(int from, int to) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  pairs_of_vertices_.emplace_back(from, to);
}

int ArcGraph::GetVerticesCount() const { return vertices_count_; }

void ArcGraph::GetNextVertices(int vertex,
                               std::vector<int>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  for (int i = 0; i < pairs_of_vertices_.size(); ++i) {
    if (pairs_of_vertices_[i].prev_vertex == vertex) {
      next_vertices.emplace_back(pairs_of_vertices_[i].next_vertex);
    }
  }
}

void ArcGraph::GetPrevVertices(int vertex,
                               std::vector<int>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(prev_vertices.empty());

  for (int i = 0; i < pairs_of_vertices_.size(); ++i) {
    if (pairs_of_vertices_[i].next_vertex == vertex) {
      prev_vertices.emplace_back(pairs_of_vertices_[i].prev_vertex);
    }
  }
}