#include "ListGraph.h"
#include <assert.h>

ListGraph::ListGraph(int vertices_count)
    : next_vertices_(vertices_count), prev_vertices_(vertices_count) {}

ListGraph::ListGraph(const IGraph* graph) {
  next_vertices_.resize(graph->GetVerticesCount());
  prev_vertices_.resize(graph->GetVerticesCount());

  for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
    graph->GetNextVertices(vertex, next_vertices_[vertex]);
    graph->GetPrevVertices(vertex, prev_vertices_[vertex]);
  }
}

ListGraph::~ListGraph() {}

void ListGraph::AddEdge(int from, int to) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  next_vertices_[from].emplace_back(to);
  prev_vertices_[to].emplace_back(from);
}

int ListGraph::GetVerticesCount() const { return next_vertices_.size(); }

void ListGraph::GetNextVertices(int vertex,
                                std::vector<int>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  next_vertices = next_vertices_[vertex];
}

void ListGraph::GetPrevVertices(int vertex,
                                std::vector<int>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(prev_vertices.empty());

  prev_vertices = prev_vertices_[vertex];
}