#include <vector>
#include "IGraph.h"

class ArcGraph : public IGraph {
 public:
  explicit ArcGraph(int vertices_count);
  explicit ArcGraph(const IGraph* graph);
  ~ArcGraph();

  virtual void AddEdge(int from, int to) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetNextVertices(
      int vertex, std::vector<int>& next_vertices) const override final;
  virtual void GetPrevVertices(
      int vertex, std::vector<int>& prev_vertices) const override final;

 private:
  struct PairOfVertices {
    int prev_vertex;
    int next_vertex;
    PairOfVertices(int from, int to){
      prev_vertex = from;
      next_vertex = to;
    }
  };
  std::vector<PairOfVertices> pairs_of_vertices_;
    int vertices_count_;
};