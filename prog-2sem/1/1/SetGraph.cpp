#include "SetGraph.h"
#include <assert.h>

SetGraph::SetGraph(int vertices_count)
    : next_vertices_(vertices_count), prev_vertices_(vertices_count) {}

SetGraph::SetGraph(const IGraph* graph) {
  next_vertices_.resize(graph->GetVerticesCount());
  prev_vertices_.resize(graph->GetVerticesCount());

  for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
    std::vector<int> next_vertices;
    graph->GetNextVertices(vertex, next_vertices);
    for (int next_vertex = 0; next_vertex < next_vertices.size();
         ++next_vertex) {
      AddEdge(vertex, next_vertex);
    }
  }
}

SetGraph::~SetGraph() {}

void SetGraph::AddEdge(int from, int to) {
  assert(from >= 0 && from < GetVerticesCount());
  assert(to >= 0 && to < GetVerticesCount());

  next_vertices_[from].insert(to);
  prev_vertices_[to].insert(from);
}

int SetGraph::GetVerticesCount() const { return next_vertices_.size(); }

void SetGraph::GetNextVertices(int vertex,
                               std::vector<int>& next_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(next_vertices.empty());

  next_vertices.assign(next_vertices_[vertex].begin(),
                       next_vertices_[vertex].end());
}

void SetGraph::GetPrevVertices(int vertex,
                               std::vector<int>& prev_vertices) const {
  assert(vertex >= 0 && vertex < GetVerticesCount());
  assert(prev_vertices.empty());

  prev_vertices.assign(prev_vertices_[vertex].begin(), prev_vertices_[vertex].end());
}