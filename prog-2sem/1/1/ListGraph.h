#include <vector>
#include "IGraph.h"

class ListGraph : public IGraph {
 public:
  explicit ListGraph(int vertices_count);
  explicit ListGraph(const IGraph* graph);
  ~ListGraph();

  virtual void AddEdge(int from, int to) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const override final;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int>& vertices) const override final;

 private:
  std::vector<std::vector<int>> next_vertices_;
  std::vector<std::vector<int>> prev_vertices_;
};