#include "MatrixGraph.h"
#include <assert.h>

MatrixGraph::MatrixGraph(int vertices_count)
                       : has_edge_(vertices_count) {
    for (int i = 0; i < vertices_count; ++i) {
        has_edge_[i].resize(vertices_count);
    }
}

MatrixGraph::MatrixGraph(const IGraph* graph) {
    has_edge_.resize(graph->GetVerticesCount());

    for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
        has_edge_[vertex].resize(GetVerticesCount());
    }
    for (int i = 0; i < GetVerticesCount(); ++i) {
        for (int j = 0; j < GetVerticesCount(); ++j) {
            has_edge_[i][j] = false;
        }
    }

    for (int vertex = 0; vertex < GetVerticesCount(); ++vertex) {
        std::vector<int> next_vertices;
        graph->GetNextVertices(vertex, next_vertices);

        for (int next_vertex = 0; next_vertex < next_vertices.size();
             ++next_vertex) {
            AddEdge(vertex, next_vertices[next_vertex]);
        }
    }
}

MatrixGraph::~MatrixGraph() {}

void MatrixGraph::AddEdge(int from, int to) {
    assert(from >= 0 && from < GetVerticesCount());
    assert(to >= 0 && to < GetVerticesCount());

    has_edge_[from][to] = true;
}

int MatrixGraph::GetVerticesCount() const { return has_edge_.size(); }

void MatrixGraph::GetNextVertices(int vertex,
                                  std::vector<int>& next_vertices) const {
    assert(vertex >= 0 && vertex < GetVerticesCount());
    assert(next_vertices.empty());

    for (int i = 0; i < GetVerticesCount(); ++i) {
        if (has_edge_[vertex][i]) {
            next_vertices.emplace_back(i);
        }
    }
}

void MatrixGraph::GetPrevVertices(int vertex,
                                  std::vector<int>& prev_vertices) const {
    assert(vertex >= 0 && vertex < GetVerticesCount());
    assert(prev_vertices.empty());

    for (int i = 0; i < GetVerticesCount(); ++i) {
        if (has_edge_[i][vertex]) {
            prev_vertices.emplace_back(i);
        }
    }
}
