#include <unordered_set>
#include "IGraph.h"

class SetGraph : public IGraph {
 public:
  explicit SetGraph(int vertices_count);
  explicit SetGraph(const IGraph* graph);
  ~SetGraph();

  virtual void AddEdge(int from, int to) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetNextVertices(
      int vertex, std::vector<int>& next_vertices) const override final;
  virtual void GetPrevVertices(
      int vertex, std::vector<int>& prev_vertices) const override final;

 private:
  std::vector<std::unordered_set<int>> next_vertices_;
  std::vector<std::unordered_set<int>> prev_vertices_;
};