#include <vector>
#include "IGraph.h"

class MatrixGraph : public IGraph {
 public:
  explicit MatrixGraph(int vertices_count);
  explicit MatrixGraph(const IGraph* graph);
  ~MatrixGraph();

  virtual void AddEdge(int from, int to) override final;

  virtual int GetVerticesCount() const override final;

  virtual void GetNextVertices(
      int vertex, std::vector<int>& next_vertices) const override final;
  virtual void GetPrevVertices(
      int vertex, std::vector<int>& prev_vertices) const override final;

 private:
  std::vector<std::vector<bool>> has_edge_;
};