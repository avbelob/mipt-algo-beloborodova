/*
Условие:
Дан невзвешенный неориентированный граф. Найдите цикл минимальной длины.
Ввод: v:кол-во вершин(макс. 50000), n:кол-во ребер(макс. 200000), n пар реберных
вершин.
Вывод: одно целое число равное длине минимального цикла. Если цикла нет,
то вывести -1.

 */
#include <iostream>
#include <optional>
#include <queue>
#include <vector>

using std::queue;
using std::vector;

class Graph {
 public:
  explicit Graph(unsigned int vertex_count)
      : vertex_count_(vertex_count), adjacency_list_(vertex_count_) {}

  void AddEdge(int from, int to);

  int GetVerticesCount() const;

  int FindMinCycle() const;

 private:
  int BFS(int vertex) const;

  int vertex_count_;

  vector<vector<int>> adjacency_list_;
};

void Graph::AddEdge(int from, int to) {
  adjacency_list_[from].push_back(to);
  adjacency_list_[to].push_back(from);
}

int Graph::GetVerticesCount() const { return vertex_count_; }

int Graph::BFS(int vertex) const {
  queue<int> vertices_queue;
  vertices_queue.push(vertex);
  vector<bool> is_vertices_visited(vertex_count_);
  vector<int> path_length(vertex_count_);
  vector<std::optional<int>> parent(vertex_count_);
  is_vertices_visited[vertex] = true;

  while (!vertices_queue.empty()) {
    int current_vertex = vertices_queue.front();
    vertices_queue.pop();

    for (size_t i = 0; i < adjacency_list_[current_vertex].size(); ++i) {
      int next_vertices = adjacency_list_[current_vertex][i];
      if (!is_vertices_visited[next_vertices]) {
        is_vertices_visited[next_vertices] = true;
        vertices_queue.push(next_vertices);
        path_length[next_vertices] = path_length[current_vertex] + 1;
        parent[next_vertices] = current_vertex;
      } else if (next_vertices != parent[current_vertex]) {
        return path_length[current_vertex] + path_length[next_vertices] + 1;
      }
    }
  }

  return vertex_count_ + 1;
}
int Graph::FindMinCycle() const {
  int min_cycle = vertex_count_ + 1;
  for (int i = 0; i < vertex_count_; i++) {
    int cycle = BFS(i);
    if (cycle < min_cycle) {
      min_cycle = cycle;
    }
  }
  return min_cycle;
}

int main() {
  int vertex_count;
  int edge_count;
  std::cin >> vertex_count;
  Graph graph(vertex_count);
  std::cin >> edge_count;

  for (int i = 0; i < edge_count; i++) {
    int vertex1;
    int vertex2;
    std::cin >> vertex1 >> vertex2;
    graph.AddEdge(vertex1, vertex2);
  }

  int min_cycle = graph.FindMinCycle();
  if (min_cycle == graph.GetVerticesCount() + 1) {
    min_cycle = -1;
  }

  std::cout << min_cycle;
}